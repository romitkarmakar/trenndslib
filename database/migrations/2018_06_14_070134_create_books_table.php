<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->increments('id');
            $table->string('isbn')->nullable();
            $table->string('title');
            $table->string('sub_title')->nullable();
            $table->string('author',80);
            $table->string('edition',80);
            $table->string('edition_year',80)->nullable();
            $table->integer('quantity');
            $table->string('publisher',80)->nullable();
            $table->string('series',80)->nullable();
            $table->integer('category_id');
            $table->string('editor',80)->nullable();
            $table->string('publication_year',80)->nullable();
            $table->string('publication_place',80)->nullable();
            $table->string('rack_no',20)->nullable();
            $table->string('barcode',20)->nullable();
            $table->string('book_id',80)->default('book_id')->unique();
            $table->text('description')->nullable();
            $table->string('book_type')->default('Physical Book');
            $table->string('ebook')->nullable();
            $table->string('photo',50)->default('book.png');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}
