$(document).ready(function(){
	$("select.select2").select2();
	$(".datepicker").datepicker();
	$(".yearpicker").datepicker({
	    format: "yyyy",
	    viewMode: "years", 
	    minViewMode: "years"
	});
	$(".data-table").DataTable({
        responsive: true,
        ordering: false
    });
    $('.dropify').dropify();
    $('.summernote').summernote({
        height: 150
    });
	$("input:required, select:required, textarea:required").prev().append("<span class='required'> *</span>");
	//delete message
	$(document).on('click','.btn-remove',function(){
		var c = confirm("Are you sure you want to permanently remove this record?");
	    if(c){
			return true;
		}
		return false;
	});

	//alert message hide
	// $(".alert").hide();
	// $(".alert").fadeTo(2000, 500).slideUp(500, function(){
	// 	$(".alert").slideUp(500);
	// }); 

	//Print Command
	$('body').on('click','.print',function(){
		var div = "#" + $(this).data("print");	
		$(div).print({
			timeout: 1000,
		});	
	});

	//Active tab
    $("#nav-tab > a").each(function () {
        var path = window.location.href;
        if ($(this).attr("href").slice(1) == path.split('/').splice(-1)[0]) {
            $("#nav-tab > a").removeClass("active show");
            $(this).addClass("active show");
            $(".tab-pane").removeClass("active show");
            $('#' + $(this).attr("href").slice(1)).addClass("active show");
        }
    });
	
	//Ajax Modal Function
	$('body').on("click",".ajax-modal",function(){
		var link = $(this).attr("href");
		var title = $(this).data("title");
		$.ajax({
			url: link,
			beforeSend: function(){
				$("#loader").css("display","block");
			},success: function(data){
				$("#loader").css("display","none");
				$('#main_modal .modal-title').html(title);
				$('#main_modal .modal-body').html(data);
				$("#main_modal .alert-success").css("display","none");
				$("#main_modal .alert-danger").css("display","none");
				$('#main_modal').modal('show'); 
				//init Essention jQuery Library
				$(".datepicker").datepicker();
				$(".yearpicker").datepicker({
				    format: "yyyy",
				    viewMode: "years", 
				    minViewMode: "years"
				});
				$("select.select2").select2();	
				$(".dropify").dropify();
				$("#main_modal input:required, #main_modal select:required, #main_modal textarea:required").prev().append("<span class='required'> *</span>");
			}
		});
		return false;
	}); 

	$("#main_modal").on('show.bs.modal', function () {
		$('#main_modal').css("overflow-y","hidden"); 		
	});

	$("#main_modal").on('shown.bs.modal', function () {
		setTimeout(function(){
			$('#main_modal').css("overflow-y","auto");
		}, 1000);	
	});


	//Ajax Modal Submit
	$(document).on("submit",".ajax-submit",function(){			 
		var link = $(this).attr("action");
		$.ajax({
			method: "POST",
			url: link,
			data:  new FormData(this),
			mimeType:"multipart/form-data",
			contentType: false,
			cache: false,
			processData:false,
			beforeSend: function(){
				$("#loader").css("display","block");
			},success: function(data){

				var json = JSON.parse(data);
				if(json['result'] == "success"){
					$("#loader").css("display","none");
					Command: toastr["success"](json['message']);
					if(typeof json['redirect'] != 'undefined'){
						window.setTimeout(function(){window.location.replace(json['redirect'])}, 1000);
					}else{
						window.setTimeout(function(){window.location.reload()}, 1000);
					}
				}else{
					$("#loader").css("display","none");
					if(typeof json['message'] === 'string'){
						Command: toastr["error"](json['message']);
					}else{
						jQuery.each( json['message'], function( i, val ) {
							Command: toastr["error"](val);
						});
					}
				}
			}
		});

		return false;
	});	 

	//Ajax Modal Submit
	$(document).on("submit",".ajax-submit-library",function(){			 
		var link = $(this).attr("action");
		$.ajax({
			method: "POST",
			url: link,
			data:  new FormData(this),
			mimeType:"multipart/form-data",
			contentType: false,
			cache: false,
			processData:false,
			beforeSend: function(){
				$("#loader").css("display","block");
			},success: function(data){
				$("#loader").css("display","none");
				var json = JSON.parse(data);
				if(json['result'] == "success"){
					Command: toastr["success"](json['message']);
					window.setTimeout(function(){window.location.replace(json['redirect'])}, 1000);
				}else{
					if(typeof json['message'] === 'string'){
						Command: toastr["error"](json['message']);
					}else{
						jQuery.each( json['message'], function( i, val ) {
							Command: toastr["error"](val);
						});
					}
				}
			}
		});

		return false;
	});
});