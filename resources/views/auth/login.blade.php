@extends('layouts.app')

@section('content')
<form action="{{ route('login') }}" method="post">
    @csrf
    @if ($errors->has('email'))
        <div class="alert alert-danger" role="alert">
            <button type="button" class="close" data-dismiss="alert">x</button>
            {{ $errors->first('email') }}
        </div>
    @endif
    <div class="form-group">
        <label>Email Address</label>
        <input id="email" type="email" class="au-input au-input--full" name="email" value="{{ old('email') }}" required autofocus>
    </div>
    <div class="form-group">
        <label>Password</label>
        <input id="password" type="password" class="au-input au-input--full" name="password" required>
    </div>
    <div class="login-checkbox">
        <label>
            <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>Remember Me
        </label>
        <label>
            @if (Route::has('password.request'))
            <a href="{{ route('password.request') }}">
                Forgot Your Password?
            </a>
            @endif
        </label>
    </div>
    <button class="au-btn au-btn--block au-btn--green m-b-20" type="submit">sign in</button>
</form>
@endsection