@extends('layouts.app')

@section('content')
@if (session('status'))
<div class="alert alert-success" role="alert">
    {{ session('status') }}
</div>
@endif
<form action="{{ route('password.email') }}" method="post">
    @csrf
    @if ($errors->has('email'))
        <div class="alert alert-danger" role="alert">
            <button type="button" class="close" data-dismiss="alert">x</button>
            {{ $errors->first('email') }}
        </div>
    @endif
    <div class="form-group">
        <label>Email Address</label>
        <input id="email" type="email" class="au-input au-input--full" name="email" value="{{ old('email') }}" required autofocus>
    </div>
    <button class="au-btn au-btn--block au-btn--green m-b-20" type="submit">Send Password Reset Link</button>
</form>
@endsection
