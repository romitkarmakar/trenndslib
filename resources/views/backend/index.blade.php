@extends('layouts.backend')

@section('content')
<div class="row m-t-25">
	<div class="col-sm-6 col-lg-3">
		<div class="overview-item overview-item--c1">
			<div class="overview__inner">
				<div class="overview-box clearfix">
					<div class="icon">
						<i class="fas fa-users"></i>
					</div>
					<div class="text">
						<h3>10000</h3>
						<span>Total Members</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-sm-6 col-lg-3">
		<div class="overview-item overview-item--c2">
			<div class="overview__inner">
				<div class="overview-box clearfix">
					<div class="icon">
						<i class="fas fa-book"></i>
					</div>
					<div class="text">
						<h3>1000000</h3>
						<span>Total Books</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-sm-6 col-lg-3">
		<div class="overview-item overview-item--c3">
			<div class="overview__inner">
				<div class="overview-box clearfix">
					<div class="icon">
						<i class="fas fa-clock"></i>
					</div>
					<div class="text">
						<h3>100000000</h3>
						<span>Issue Books</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-sm-6 col-lg-3">
		<div class="overview-item overview-item--c4">
			<div class="overview__inner">
				<div class="overview-box clearfix">
					<div class="icon">
						<i class="fas fa-reply"></i>
					</div>
					<div class="text">
						<h3>100000000</h3>
						<span>Return Books</span>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection