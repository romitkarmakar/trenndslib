<div class="table-responsive">
	<table class="table table-bordered table-view">
		<tr class="text-center">
			<td colspan="2"><img src="{{  asset('public/uploads/images/' . $user->image) }}"></td>
		</tr>
		<tr>
			<td>Name</td>
			<td>{{ $user->name }}</td>
		</tr>
		<tr>
			<td>Phone</td>
			<td>{{ $user->phone }}</td>
		</tr>
		<tr>
			<td>Email</td>
			<td>{{ $user->email }}</td>
		</tr>
		<tr>
			<td>Role</td>
			<td>{{ $user->role }}</td>
		</tr>
		<tr>
			<td>Address</td>
			<td>{{ $user->address }}</td>
		</tr>
	</table>
</div>
