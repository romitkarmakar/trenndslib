@extends('layouts.backend')
@section('content')
<div class="row">
	<div class="col-md-8">
		<div class="card">
			<div class="card-header with-border">
				<strong class="card-title float-left">Create</strong>
				<a href="{{ route('users.index') }}" style="color: #000;" class="float-right">
					<i class="fas fa-reply"></i>
				</a>
			</div>
			<div class="card-body">
				<form action="{{ route('users.store') }}" class="form-horizontal form-groups-bordered validate" enctype="multipart/form-data" method="post" accept-charset="utf-8">
					@csrf
					
					<div class="col-md-6">
						<div class="form-group">
							<label class="form-control-label">Name</label>
							<input type="text" name="name" class="form-control" value="{{ old('name') }}" placeholder="" required>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="form-control-label">Phone</label>
							<input type="text" name="phone" class="form-control" value="{{ old('phone') }}">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="form-control-label">Email</label>
							<input type="text" name="email" class="form-control" value="{{ old('email') }}" required>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="form-control-label">Role</label>
							<select class="form-control select2" name="role" required>
								<option value="">Select One</option>
								<option {{ (old('role') == 'Admin') ? 'selected' : ''}} value="Admin">Admin</option>
								<option {{ (old('role') == 'Librarian') ? 'selected' : ''}} value="Librarian">Librarian</option>
							</select>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label class="form-control-label">Address </label>
							<textarea class="form-control" name="address" required>{{ old('address') }}</textarea>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="form-control-label">Password</label>
							<input type="password" name="password" class="form-control" value="{{ old('password') }}" placeholder="" required>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="form-control-label">Password Confirmation</label>
							<input type="password" name="password_confirmation" class="form-control" value="{{ old('password_confirmation') }}" placeholder="" required>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label">Image</label>						
							<input type="file" class="form-control dropify" name="image">
						</div>
					</div>
					<div class="col-md-12 text-right">
						<button type="submit" class="btn btn-success">Add</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div> 
@endsection
