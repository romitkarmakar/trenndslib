@extends('layouts.backend')
@section('content')
<div class="row">
	<div class="col-md-6">
		<div class="card">
			<div class="card-header">
				<strong class="card-title float-left">Edit</strong>
				<a href="{{ route('users.index') }}" style="color: #000;" class="float-right">
					<i class="fas fa-reply"></i>
				</a>
			</div>
			<div class="card-body">
				<div class="mx-auto d-block">
					<div class="table-responsive">
						<table class="table table-bordered table-view">
							<tr class="text-center">
								<td colspan="2"><img src="{{  asset('public/uploads/images/' . $user->image) }}"></td>
							</tr>
							<tr>
								<td>Name</td>
								<td>{{ $user->name }}</td>
							</tr>
							<tr>
								<td>Phone</td>
								<td>{{ $user->phone }}</td>
							</tr>
							<tr>
								<td>Email</td>
								<td>{{ $user->email }}</td>
							</tr>
							<tr>
								<td>Role</td>
								<td>{{ $user->role }}</td>
							</tr>
							<tr>
								<td>Address</td>
								<td>{{ $user->address }}</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
