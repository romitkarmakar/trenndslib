@extends('layouts.backend')
@section('content')
<div class="row">
	<div class="col-md-8">
		<div class="card">
			<div class="card-header with-border">
				<strong class="card-title float-left">Edit</strong>
				<a href="{{ route('users.index') }}" style="color: #000;" class="float-right">
					<i class="fas fa-reply"></i>
				</a>
			</div>
			<div class="card-body">
				<form action="{{ route('users.update',$user->id) }}" class="form-horizontal form-groups-bordered validate" enctype="multipart/form-data" method="post" accept-charset="utf-8">
					@csrf
					@method('PUT')
					<div class="col-md-6">
						<div class="form-group">
							<label class="form-control-label">Name</label>
							<input type="text" name="name" class="form-control" value="{{ $user->name }}" required>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="form-control-label">Phone</label>
							<input type="text" name="phone" class="form-control" value="{{ $user->phone }}">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="form-control-label">Email</label>
							<input type="text" name="email" class="form-control" value="{{ $user->email }}" required>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="form-control-label">Role</label>
							<select class="form-control select2" name="role" required>
								<option {{ ($user->role == 'Admin') ? 'selected' : ''}} value="Admin">Admin</option>
								<option {{ ($user->role == 'Librarian') ? 'selected' : ''}} value="Librarian">Librarian</option>
							</select>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label class="form-control-label">Address </label>
							<textarea class="form-control" name="address" required>{{ $user->address }}</textarea>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label">Image</label>						
							<input type="file" class="form-control dropify" name="image" data-default-file="{{ ($user->image != '') ? asset('public/uploads/images/' . $user->image) : '' }}">
						</div>
					</div>
					<div class="col-md-12 text-right">
						<button type="submit" class="btn btn-success">Update</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection
