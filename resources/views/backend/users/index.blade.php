@extends('layouts.backend')
@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-header">
				<strong class="card-title float-left">Users List</strong>
				<a href="{{ route('users.create') }}" class="btn btn-primary btn-sm float-right ajax-modal" data-title="Create">
					<i class="fa fa-plus"></i> Add New
				</a>
			</div>
			<div class="card-body">
				<table class="table table-striped table-bordered data-table" style="width:100%">
					<thead>
						<tr>
							<th>Profile</th>	
							<th>Name</th>
							<th>Email</th>
							<th>Role</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						@php $i = 1; @endphp
						@foreach ($users as $data)
						<tr>
							<td><img class="profile" src="{{  asset('public/uploads/images/' . $data->image) }}"></td>
							<td>{{ $data->name }}</td>
							<td>{{ $data->email }}</td>
							<td>{{ $data->role }}</td>
							<td>
								<form action="{{ route('users.destroy',$data->id) }}" method="post">
									<a href="{{ route('users.show',$data->id) }}" class="btn btn-success btn-sm btn-custom ajax-modal" data-title="Show"><i class="fa fa-eye" style="color: #fff" aria-hidden="true"></i></a>
									<a href="{{ route('users.edit',$data->id) }}" class="btn btn-warning btn-sm btn-custom ajax-modal" data-title="Edit"><i class="fa fa-pencil-alt" style="color: #fff" aria-hidden="true"></i></a>
									@method('DELETE')
									@csrf
									<button type="submit" class="btn btn-danger btn-sm btn-custom btn-remove"><i class="fas fa-eraser" aria-hidden="true"></i></button>
								</form>
							</td>
						</tr>
						@php $i++; @endphp
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection
