@extends('layouts.backend')
@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-header">
				<h4>Recents Notifications</h4>
			</div>
			<div class="card-body">
				<div class="default-tab tabs-left">
					<nav>
						<div class="nav nav-tabs nav-justified" id="nav-tab" role="tablist">
							<a class="nav-item nav-link active show" id="nav-home-tab" data-toggle="tab" href="#notifications" role="tab" aria-controls="nav-home" aria-selected="true">Notifications</a>
							<a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#emails" role="tab" aria-controls="nav-contact" aria-selected="false">Emails</a>
						</div>
					</nav>
					<div class="tab-content pt-2" id="nav-tabContent">
						<div class="tab-pane fade active show" id="notifications" role="tabpanel">
							<div class="card">
								<div class="card-body">
									<table class="table table-striped table-bordered data-table" style="width:100%">
										<thead>
											<tr>	
												<th>Member</th>
												<th>Subject</th>
												<th>Time</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
											@php $i = 1; @endphp
											@foreach ($notifications as $data)
											<tr>
												<td>
													Id - {{ $data->circulation->member->member_id }}</br>
													Name - {{ $data->circulation->member->user->name }}</br>
													Email - {{ $data->circulation->member->user->email }}
												</td>
												<td>{{ $data->subject }}</td>
												<td>{{ $data->created_at }}</td>
												<td>
													<a href="{{ route('notifications.show',$data->id) }}" class="btn btn-success btn-sm  ajax-modal" data-title="Details"><i class="fa fa-eye" style="color: #fff" aria-hidden="true"></i></a>
												</td>
											</tr>
											@php $i++; @endphp
											@endforeach
										</tbody>
									</table>
								</div>
							</div>
						</div>
						<div class="tab-pane fade" id="emails" role="tabpanel">
							<div class="card">
								<div class="card-body">
									<table class="table table-striped table-bordered data-table" style="width:100%">
										<thead>
											<tr>	
												<th>Member</th>
												<th>Subject</th>
												<th>Time</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
											@php $i = 1; @endphp
											@foreach ($emails as $data)
											<tr>
												<td>
													Id - {{ $data->circulation->member->member_id }}</br>
													Name - {{ $data->circulation->member->user->name }}</br>
													Email - {{ $data->circulation->member->user->email }}
												</td>
												<td>{{ $data->subject }}</td>
												<td>{{ $data->created_at }}</td>
												<td>
													<a href="{{ route('notifications.show',$data->id) }}" class="btn btn-success btn-sm  ajax-modal" data-title="Details"><i class="fa fa-eye" style="color: #fff" aria-hidden="true"></i></a>
												</td>
											</tr>
											@php $i++; @endphp
											@endforeach
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection