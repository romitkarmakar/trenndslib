<div class="table-responsive">
	<table class="table table-bordered table-view">
		<tr>
			<th>Member Name</th>
			<td>{{ $notification->circulation->member->user->name }}</td>
		</tr>
		<tr>
			<th>Member Id</th>
			<td>{{ $notification->circulation->member->member_id }}</td>
		</tr>
		<tr>
			<th>Subject</th>
			<td>{{ $notification->subject }}</td>
		</tr>
		<tr>
			<th>Message</th>
			<td>{!! get_message($notification->id) !!}</td>
		</tr>
		<tr>
			<th>Sended By</th>
			<td>{{ $notification->user->name }}</td>
		</tr>
		<tr>
			<th>Sended At</th>
			<td>{{ $notification->created_at }}</td>
		</tr>
	</table>
</div>