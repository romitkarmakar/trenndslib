@extends('layouts.backend')
@section('content')
<div class="row">
	<div class="col-md-8">
		<div class="card">
			<div class="card-header with-border">
				<strong class="card-title float-left">Notification</strong>
			</div>
			<div class="card-body">
				<form action="{{ route('notifications.store') }}" class="form-horizontal form-groups-bordered validate" method="post" accept-charset="utf-8">
					@csrf
					<input type="hidden" name="circulation_id" value="{{ $circulation_id }}">
					<div class="col-md-12">
						<div class="form-group">
							<label class="form-control-label">Notification Type</label>
							<select class="form-control select2" name="type" required>
								<option value="1">Only Notification</option>
								<option value="2">Only Email</option>
								<option value="3">Notification And Email</option>
							</select>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label class="form-control-label">Notification Subject</label>
							<input type="text" name="subject" class="form-control" value="{{ app_data('notification_subject') }}" required>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label class="form-control-label">Notification Message</label>
							<textarea class="form-control" name="message" rows="6" required>{{ app_data('notification_message') }}</textarea>
						</div>
					</div>
					<div class="col-md-12 text-right">
						<button type="submit" class="btn btn-success">Send</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="alert alert-info">
			<strong>Notice!</strong>You can also use those dynamic codes in message. ($member_name, $member_id, $book_title, $book_id, $issue_date, $due_date).</br>
			Example - hello $member_name output will hello john doe(member name)
		</div>
	</div>
</div> 
@endsection
