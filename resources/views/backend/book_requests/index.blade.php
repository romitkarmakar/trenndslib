@extends('layouts.backend')
@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-header">
				<strong class="card-title float-left">Book Requests List</strong>
			</div>
			<div class="card-body">
				<table class="table table-striped table-bordered data-table" style="width:100%">
					<thead>
						<tr>
							<th>Member</th>	
							<th>Book Title</th>
							<th>Requested</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						@php $i = 1; @endphp
						@foreach ($book_requests as $data)
						<tr>
							<td>
								ID - {{ $data->member->member_id }} </br>
								Name - {{ $data->member->user->name }}
							</td>
							<td>{{ $data->book_title }}</td>
							<td>{{ date_format($data->created_at,"Y-m-d") }}</td>
							<td>
								@if($data->status == 0)
								<a href="{{ url('book_requests/status/' . $data->id . '/Accept') }}" class="badge badge-success">Accept</a>
								<a href="{{ url('book_requests/status/' . $data->id . '/Reject') }}" class="badge badge-danger">Reject</a>
								@else
								@if($data->status == 1)
								<span class="badge badge-success">Accepted</span>
								@elseif($data->status ==2)
								<span class="badge badge-danger">Rejected</span>
								@endif
								@endif
							</td>
							<td>
								<a href="{{ url('book_requests/' . $data->id) }}" class="btn btn-success btn-custom  ajax-modal" data-title="Details"><i class="fa fa-eye" style="color: #fff" aria-hidden="true"></i></a>
							</td>
						</tr>
						@php $i++; @endphp
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection
