@extends('layouts.backend')
@section('content')
<div class="row">
	<div class="col-md-8">
		<div class="card">
			<div class="card-header with-border">
				<strong class="card-title float-left">Details</strong>
			</div>
			<div class="card-body">
				<div class="table-responsive">
					<table class="table table-bordered table-view">
						<tr>
							<td colspan="4" class="text-center"><img src="{{  asset('public/uploads/images/' . $book->photo) }}"></td>
						</tr>
						<tr>
							<th>ISBN</th>
							<td>{{ $book->isbn }}</td>
							<th>Book Id</th>
							<td>{{ $book->book_id }}</td>
						</tr>
						<tr>
							<th>Title</th>
							<td>{{ $book->title }}</td>
							<th>Sub Title</th>
							<td>{{ $book->sub_title }}</td>
						</tr>
						<tr>
							<th>Author</th>
							<td>{{ $book->author }}</td>
							<th>Edition</th>
							<td>{{ $book->edition }}</td>
						</tr>
						<tr>
							<th>Edition Year</th>
							<td>{{ $book->edition_year }}</td>
							<th>Quantity</th>
							<td>{{ $book->quantity }}</td>
						</tr>
						<tr>
							<th>Publisher</th>
							<td>{{ $book->publisher }}</td>
							<th>Series</th>
							<td>{{ $book->series }}</td>
						</tr>
						<tr>
							<th>Category</th>
							<td>{{ $book->category->category_name }}</td>
							<th>Editor</th>
							<td>{{ $book->editor }}</td>
						</tr>
						<tr>
							<th>Publication Year</th>
							<td>{{ $book->publication_year }}</td>
							<th>Publication Place</th>
							<td>{{ $book->publication_place }}</td>
						</tr>
						@if($book->description == '')
						<tr>
							<th>Barcode</th>
							<td>{{ $book->barcode }}</td>
							<th>Description</th>
							<td>{{ $book->description }}</td>
						</tr>
						@else
						<tr>
							<th colspan="2">Barcode</th>
							<td colspan="2">{{ $book->barcode }}</td>
						</tr>
						<tr>
							<th colspan="2">Description</th>
							<td colspan="2">{{ $book->description }}</td>
						</tr>
						@endif
					</table>
				</div>
			</div>
		</div>
	</div>
</div> 
@endsection
