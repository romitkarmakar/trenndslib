@extends('layouts.backend')
@section('content')
<div class="row">
	<div class="col-md-8">
		<div class="card">
			<div class="card-header with-border">
				<strong class="card-title float-left">Edit</strong>
				<a href="{{ route('books.index') }}" style="color: #000;" class="float-right">
					<i class="fas fa-reply"></i>
				</a>
			</div>
			<div class="card-body">
				<form action="{{route('books.update',$book->id)}}" class="form-horizontal form-groups-bordered validate" enctype="multipart/form-data" method="post" accept-charset="utf-8">
					@csrf
					@method('PUT')
					<div class="col-md-6">
						<div class="form-group">
							<label class="form-control-label">ISBN</label>
							<input type="text" name="isbn" class="form-control" value="{{ $book->isbn }}">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="form-control-label">Title</label>
							<input type="text" name="title" class="form-control" value="{{ $book->title }}" required>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="form-control-label">Sub Title</label>
							<input type="text" name="sub_title" class="form-control" value="{{ $book->sub_title }}">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="form-control-label">Author</label>
							<input type="text" name="author" class="form-control" value="{{ $book->author }}" required>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="form-control-label">Edition</label>
							<input type="text" name="edition" class="form-control" value="{{ $book->edition }}" required>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="form-control-label">Edition Year</label>
							<input type="text" name="edition_year" class="form-control" value="{{ $book->edition_year }}">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="form-control-label">Quantity</label>
							<input type="number" name="quantity" class="form-control" value="{{ $book->quantity }}"required>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="form-control-label">Publisher</label>
							<input type="text" name="publisher" class="form-control" value="{{ $book->publisher }}"required>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="form-control-label">Series</label>
							<input type="text" name="series" class="form-control" value="{{ $book->series }}">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="form-control-label">Category</label>
							<select class="form-control select2" name="category_id" required>
								{{ create_option('book_categories', 'id', 'category_name', $book->category_id) }}
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="form-control-label">Editor</label>
							<input type="text" name="editor" class="form-control" value="{{ $book->editor }}">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="form-control-label">Publication Year</label>
							<input type="text" name="publication_year" class="form-control" value="{{ $book->publication_year }}">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="form-control-label">Publication Place</label>
							<input type="text" name="publication_place" class="form-control" value="{{ $book->publication_place }}">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="form-control-label">Barcode</label>
							<input type="text" name="barcode" class="form-control" value="{{ $book->barcode }}">
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label class="form-control-label">Description</label>
							<textarea class="form-control" name="description">{{ $book->description }}</textarea>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label">Photo</label>						
							<input type="file" class="form-control dropify" name="photo" data-default-file="{{ ($book->photo != '') ? asset('public/uploads/images/' . $book->photo) : '' }}">
						</div>
					</div>
					<div class="col-md-12 text-right">
						<button type="submit" class="btn btn-success">Update</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div> 
@endsection

