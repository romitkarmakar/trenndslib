<form action="{{ route('book_categories.update',$category->id) }}" class="form-horizontal form-groups-bordered ajax-submit" enctype="multipart/form-data" method="post" accept-charset="utf-8">
	@csrf
	@method('PUT')
	<div class="col-md-12">
		<div class="form-group">
			<label class="form-control-label">Category Name</label>
			<input type="text" name="category_name" class="form-control" value="{{ $category->category_name }}" required>
		</div>
	</div>
	<div class="col-md-12 text-right">
		<button type="submit" class="btn btn-success">Update</button>
	</div>
</form>