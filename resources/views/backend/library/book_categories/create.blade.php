@extends('layouts.backend')
@section('content')
<div class="row">
	<div class="col-md-8">
		<div class="card">
			<div class="card-header with-border">
				<strong class="card-title float-left">Create</strong>
				<a href="{{ route('book_categories.index') }}" style="color: #000;" class="float-right">
					<i class="fas fa-reply"></i>
				</a>
			</div>
			<div class="card-body">
				<form action="{{route('book_categories.store')}}" class="form-horizontal form-groups-bordered validate" enctype="multipart/form-data" method="post" accept-charset="utf-8">
					@csrf
					
					<div class="col-md-12">
						<div class="form-group">
							<label class="form-control-label">Category Name</label>
							<input type="text" name="category_name" class="form-control" value="{{ old('category_name') }}" placeholder="" required>
						</div>
					</div>
					<div class="col-md-12 text-right">
						<button type="submit" class="btn btn-success">Add</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div> 
@endsection
