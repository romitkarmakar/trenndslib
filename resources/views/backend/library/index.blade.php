@extends('layouts.backend')
@section('content')
<div class="row">
	<div class="col-lg-12">
		<div class="card">
			<div class="card-header">
				<strong class="card-title">Library</strong>
			</div>
			<div class="card-body">
				<div id="library" class="default-tab tabs-left">
					<nav>
						<div class="nav nav-tabs nav-justified" id="nav-tab" role="tablist">
							<a class="nav-item nav-link active show" id="nav-home-tab" data-toggle="tab" href="#book_categories" role="tab" aria-controls="nav-home" aria-selected="true">Book Categories</a>
							<a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#books" role="tab" aria-controls="nav-profile" aria-selected="false">Books</a>
							<a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#members" role="tab" aria-controls="nav-contact" aria-selected="false">Members</a>
							<a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#circulations" role="tab" aria-controls="nav-contact" aria-selected="false">Circulations</a>
						</div>
					</nav>
					<div class="tab-content pt-2" id="nav-tabContent">
						<div class="tab-pane fade active show" id="book_categories" role="tabpanel">
							<div class="card">
								<div class="card-header">
									<strong class="card-title float-left">List</strong>
									<a href="{{ route('book_categories.create') }}" class="btn btn-primary btn-sm float-right ajax-modal" data-title="Create">
										<i class="fa fa-plus"></i> Add New
									</a>
								</div>
								<div class="card-body">
									<table class="table table-striped table-bordered data-table" style="width:100%">
										<thead>
											<tr>
												<th>#</th>	
												<th>Name</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
											@php $i = 1; @endphp
											@foreach ($book_categories as $data)
											<tr>
												<td>{{ $i }}</td>
												<td>{{ $data->category_name }}</td>
												<td>
													<form action="{{ route('book_categories.destroy',$data->id) }}" method="post">
														<a href="{{ route('book_categories.edit',$data->id) }}" class="btn btn-warning btn-custom  ajax-modal" data-title="Edit"><i class="fa fa-pencil-alt" style="color: #fff" aria-hidden="true"></i></a>
														@method('DELETE')
														@csrf
														<button type="submit" class="btn btn-danger btn-custom btn-remove"><i class="fas fa-eraser" aria-hidden="true"></i></button>
													</form>
												</td>
											</tr>
											@php $i++; @endphp
											@endforeach
										</tbody>
									</table>
								</div>
							</div>
						</div>
						<div class="tab-pane fade" id="books" role="tabpanel">
							<div class="card">
								<div class="card-header">
									<strong class="card-title float-left">List</strong>
									<a href="{{ route('books.create') }}" class="btn btn-primary btn-sm float-right ajax-modal" data-title="Create">
										<i class="fa fa-plus"></i> Add New
									</a>
								</div>
								<div class="card-body">
									<table class="table table-striped table-bordered data-table" style="width:100%">
										<thead>
											<tr>
												<th>Photo</th>	
												<th>Title</th>
												<th>Category</th>
												<th>Book Id</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
											@php $i = 1; @endphp
											@foreach ($books as $data)
											<tr>
												<td><img class="profile" src="{{  asset('public/uploads/images/' . $data->photo) }}"></td>
												<td>{{ $data->title }}</td>
												<td>{{ $data->category->category_name }}</td>
												<td>{{ $data->book_id }}</td>
												<td>
													<form action="{{ route('books.destroy',$data->id) }}" method="post">
														<a href="{{ route('books.show',$data->id) }}" class="btn btn-success btn-custom  ajax-modal" data-title="Details"><i class="fa fa-eye" style="color: #fff" aria-hidden="true"></i></a>
														<a href="{{ route('books.edit',$data->id) }}" class="btn btn-warning btn-custom  ajax-modal" data-title="Edit"><i class="fa fa-pencil-alt" style="color: #fff" aria-hidden="true"></i></a>
														@method('DELETE')
														@csrf
														<button type="submit" class="btn btn-danger btn-custom btn-remove"><i class="fas fa-eraser" aria-hidden="true"></i></button>
													</form>
												</td>
											</tr>
											@php $i++; @endphp
											@endforeach
										</tbody>
									</table>
								</div>
							</div>
						</div>
						<div class="tab-pane fade" id="members" role="tabpanel">
							<div class="card">
								<div class="card-header">
									<strong class="card-title float-left">List</strong>
									<a href="{{ route('members.create') }}" class="btn btn-primary btn-sm float-right ajax-modal" data-title="Create">
										<i class="fa fa-plus"></i> Add New
									</a>
								</div>
								<div class="card-body">
									<table class="table table-striped table-bordered data-table" style="width:100%">
										<thead>
											<tr>
												<th>Profile</th>	
												<th>Name</th>
												<th>Member Id</th>
												<th>Member Type</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
											@foreach ($members as $data)
											<tr>
												<td><img class="profile" src="{{  asset('public/uploads/images/' . $data->user->image) }}"></td>
												<td>{{ $data->user->name }}</td>
												<td>{{ $data->member_id }}</td>
												<td>{{ $data->member_type }}</td>
												<td class="text-center">
													<form action="{{ route('members.destroy',$data->id) }}" method="post">
														<a href="{{ route('members.library_card',$data->id) }}" class="btn btn-info btn-custom ajax-modal" data-title="Library Card"><i class="fas fa-id-card" style="color: #fff" aria-hidden="true"></i></a>
														<a href="{{ route('members.show',$data->id) }}" class="btn btn-success btn-custom ajax-modal" data-title="Details"><i class="fas fa-eye" style="color: #fff" aria-hidden="true"></i></a>
														<hr>
														<a href="{{ route('members.edit',$data->id) }}" class="btn btn-warning btn-custom ajax-modal" data-title="Edit"><i class="fas fa-pencil-alt" style="color: #fff" aria-hidden="true"></i></a>
														@method('DELETE')
														@csrf
														<button type="submit" class="btn btn-danger btn-custom btn-remove"><i class="fas fa-eraser" aria-hidden="true"></i></button>
													</form>
												</td>
											</tr>
											@endforeach
										</tbody>
									</table>
								</div>
							</div>
						</div>
						<div class="tab-pane fade" id="circulations" role="tabpanel">
							<div class="card search-box">
								<div class="card-body">
									<form action="{{ url('library/circulations/search') }}" method="post">
										@csrf
										<div class="col-md-6 offset-md-2">
											<div class="form-group">
												<select class="form-control select2" name="member_id">
													{{ create_member_option('member_id') }}
												</select>
											</div>
										</div>
										<div class="col-md-2 text-right">
											<button type="submit" class="btn btn-success btn-block">Search</button>
										</div>
									</form>
								</div>
							</div>
							<div class="card">
								<div class="card-header">
									<strong class="card-title float-left">Recents Circulations</strong>
									<a href="{{ route('circulations.create') }}" class="btn btn-primary btn-sm float-right ajax-modal" data-title="Add Issue">
										<i class="fa fa-plus"></i> Add Issue
									</a>
								</div>
								<div class="card-body">
									<table class="table table-striped table-bordered data-table" style="width:100%">
										<thead>
											<tr>
												<th>Member Id</th>	
												<th>Book ID</th>
												<th>Issue date</th>
												<th>Due Date</th>
												<th>Return Date</th>
												<th>Status</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
											@php $i = 1; @endphp
											@foreach ($circulations as $data)
											<tr>
												<td>{{ $data->member->member_id }}</td>
												<td>{{ $data->book->book_id }}</td>
												<td>{{ $data->issue_date }}</td>
												<td>{{ $data->due_date }}</td>
												<td>{{ $data->return_date }}</td>
												<td>
													@if($data->is_return == 1)
													<span class="badge badge-primary">Return</span>
													@elseif($data->is_return ==0)
													<span class="badge badge-danger">Not Return</span>
													@endif
												</td>
												<td style="text-align: center;">
													<form action="{{ route('circulations.destroy',$data->id) }}" method="post">
														<a href="{{ route('circulations.show',$data->id) }}" class="btn btn-success btn-custom ajax-modal" data-title="Details"><i class="fas fa-eye" style="color: #fff" aria-hidden="true"></i></a>
														<a href="{{ route('circulations.edit',$data->id) }}" class="btn btn-warning btn-custom ajax-modal" data-title="Edit"><i class="fas fa-pencil-alt" style="color: #fff" aria-hidden="true"></i></a>
														@if($data->is_return == 0)
														<a href="{{ url('library/circulations/book_return_info/' . $data->id) }}" class="btn btn-primary btn-custom ajax-modal" data-title="Book Return Information"><i class="fas fa-reply" style="color: #fff" aria-hidden="true"></i></a>
														@endif
														<hr>
														<a href="{{ url('notifications/create/' . $data->id) }}" class="btn btn-info btn-custom ajax-modal" data-title="Edit"><i class="fas fa-envelope" style="color: #fff" aria-hidden="true"></i></a>
														@method('DELETE')
														@csrf
														<button type="submit" class="btn btn-danger btn-custom btn-remove"><i class="fas fa-eraser" aria-hidden="true"></i></button>
													</form>
												</td>
											</tr>
											@php $i++; @endphp
											@endforeach
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection