@extends('layouts.backend')
@section('content')
<div class="row">
	<div class="col-md-6">
		<div class="card">
			<div class="card-header with-border">
				<strong class="card-title float-left">Edit</strong>
				<a href="{{ route('circulations.index') }}" style="color: #000;" class="float-right">
					<i class="fas fa-reply"></i>
				</a>
			</div>
			<div class="card-body">
				<form action="{{route('circulations.update',$book_issue->id)}}" class="form-horizontal form-groups-bordered validate" enctype="multipart/form-data" method="post" accept-charset="utf-8">
					@csrf
					@method('PUT')
					<div class="col-md-12">
						<div class="form-group">
							<label class="form-control-label">Member Id</label>
							<select class="form-control select2" name="member_id" required>
								{{ create_member_option('id') }}
							</select>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label class="form-control-label">Book Id</label>
							<select class="form-control select2" name="book_id" required>
								{{ create_option('books', 'id', 'book_id', $book_issue->book_id) }}
							</select>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label class="form-control-label">Issue Date</label>
							<input type="text" name="issue_date" class="form-control datepicker" value="{{ $book_issue->issue_date }}" readonly required>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label class="form-control-label">Due Date</label>
							<input type="text" name="due_date" class="form-control datepicker" value="{{ $book_issue->due_date }}" readonly required>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label class="form-control-label">Note</label>
							<textarea class="form-control" name="note" >{{ $book_issue->note }}</textarea>
						</div>
					</div>
					<div class="col-md-12 text-right">
						<button type="submit" class="btn btn-success">Update</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div> 
@endsection
@section('js-script')
<script type="text/javascript">
	$('select[name=member_id]').val('{{ $book_issue->member_id }}');
	$('select[name=book_id]').val('{{ $book_issue->book_id }}');
</script>
@stop
