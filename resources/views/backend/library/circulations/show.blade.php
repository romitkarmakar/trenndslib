@extends('layouts.backend')
@section('content')
<div class="row">
	<div class="col-lg-8">
		<div class="card">
			<div class="card-header">
				<h4>Details</h4>
			</div>
			<div class="card-body">
				<div class="default-tab tabs-left">
					<nav>
						<div class="nav nav-tabs nav-justified" id="nav-tab" role="tablist">
							<a class="nav-item nav-link active show" id="nav-home-tab" data-toggle="tab" href="#circulation" role="tab" aria-controls="nav-home" aria-selected="true">Circulation</a>
							<a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#member" role="tab" aria-controls="nav-contact" aria-selected="false">Member</a>
							<a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#book" role="tab" aria-controls="nav-profile" aria-selected="false">Book</a>
						</div>
					</nav>
					<div class="tab-content pt-2" id="nav-tabContent">
						<div class="tab-pane fade active show" id="circulation" role="tabpanel">
							<div class="card">
								<div class="card-body">
									<div class="table-responsive">
										<table class="table table-bordered table-view">
											<tr>
												<th>Issue Date</th>
												<td>{{ $circulation->issue_date }}</td>
											</tr>
											<tr>
												<th>Due Date</th>
												<td>{{ $circulation->due_date }}</td>
											</tr>
											<tr>
												<th>Status</th>
												<td>
													@if($circulation->is_return == 1)
													<span class="badge badge-primary">Return</span>
													@elseif($circulation->is_return ==0)
													<span class="badge badge-danger">Not Return</span>
													@endif
												</td>
											</tr>
											@if($circulation->is_return == 1)
											<tr>
												<th>Return Date</th>
												<td>{{ $circulation->return_date }}</td>
											</tr>
											@endif
											<tr>
												<th>Note</th>
												<td>{{ $circulation->note }}</td>
											</tr>
										</table>
									</div>
								</div>
							</div>
						</div>
						<div class="tab-pane fade" id="member" role="tabpanel">
							<div class="card">
								<div class="card-body">
									<div class="table-responsive">
										<table class="table table-bordered table-view">
											<tr>
												<td colspan="2"><img src="{{  asset('public/uploads/images/' . $circulation->member->user->image) }}"></td>
											</tr>
											<tr>
												<th>Name</th>
												<td>{{ $circulation->member->user->name }}</td>
											</tr>
											<tr>
												<th>Phone</th>
												<td>{{ $circulation->member->user->phone }}</td>
											</tr>
											<tr>
												<th>Email</th>
												<td>{{ $circulation->member->user->email }}</td>
											</tr>
											<tr>
												<th>Member Type</th>
												<td>{{ $circulation->member->member_type }}</td>
											</tr>
											<tr>
												<th>Address</th>
												<td>{{ $circulation->member->address }}</td>
											</tr>
										</table>
									</div>
								</div>
							</div>
						</div>
						<div class="tab-pane fade" id="book" role="tabpanel">
							<div class="card">
								<div class="card-body">
									<div class="table-responsive">
										<table class="table table-bordered table-view">
											<tr>
												<td colspan="4" class="text-center"><img src="{{  asset('public/uploads/images/' . $circulation->book->photo) }}"></td>
											</tr>
											<tr>
												<th>ISBN</th>
												<td>{{ $circulation->book->isbn }}</td>
												<th>Book Id</th>
												<td>{{ $circulation->book->book_id }}</td>
											</tr>
											<tr>
												<th>Title</th>
												<td>{{ $circulation->book->title }}</td>
												<th>Sub Title</th>
												<td>{{ $circulation->book->sub_title }}</td>
											</tr>
											<tr>
												<th>Author</th>
												<td>{{ $circulation->book->author }}</td>
												<th>Edition</th>
												<td>{{ $circulation->book->edition }}</td>
											</tr>
											<tr>
												<th>Edition Year</th>
												<td>{{ $circulation->book->edition_year }}</td>
												<th>Quantity</th>
												<td>{{ $circulation->book->quantity }}</td>
											</tr>
											<tr>
												<th>Publisher</th>
												<td>{{ $circulation->book->publisher }}</td>
												<th>Series</th>
												<td>{{ $circulation->book->series }}</td>
											</tr>
											<tr>
												<th>Category</th>
												<td>{{ $circulation->book->category->category_name }}</td>
												<th>Editor</th>
												<td>{{ $circulation->book->editor }}</td>
											</tr>
											<tr>
												<th>Publication Year</th>
												<td>{{ $circulation->book->publication_year }}</td>
												<th>Publication Place</th>
												<td>{{ $circulation->book->publication_place }}</td>
											</tr>
											@if($circulation->book->description == '')
											<tr>
												<th>Barcode</th>
												<td>{{ $circulation->book->barcode }}</td>
												<th>Description</th>
												<td>{{ $circulation->book->description }}</td>
											</tr>
											@else
											<tr>
												<th colspan="2">Barcode</th>
												<td colspan="2">{{ $circulation->book->barcode }}</td>
											</tr>
											<tr>
												<th colspan="2">Description</th>
												<td colspan="2">{{ $circulation->book->description }}</td>
											</tr>
											@endif
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection