@extends('layouts.backend')
@section('content')
<div class="row">
	<div class="col-md-6">
		<div class="card">
			<div class="card-header with-border">
				<strong class="card-title float-left">Book Return Information</strong>
				<a href="{{ route('circulations.index') }}" style="color: #000;" class="float-right">
					<i class="fas fa-reply"></i>
				</a>
			</div>
			<div class="card-body">
				<form action="{{ url('library/circulations/book_return/' . $book_issue->id) }}" class="form-horizontal form-groups-bordered" enctype="multipart/form-data" method="post" accept-charset="utf-8">
					@csrf
					<div class="col-md-12">
						<div class="form-group">
							<label class="form-control-label">Member Id</label>
							<input type="text" name="member_id" class="form-control datepicker" value="{{ $book_issue->member->member_id }}" readonly required>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label class="form-control-label">Book Id</label>
							<input type="text" name="book_id" class="form-control datepicker" value="{{ $book_issue->book->book_id }}" readonly required>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label class="form-control-label">Return Date</label>
							<input type="text" name="return_date" class="form-control datepicker" value="{{ (old('return_date') != '') ? old('return_date') : \Carbon\Carbon::now()->toDateString() }}" readonly required>
						</div>
					</div>
					<div class="col-md-12 text-right">
						<button type="submit" class="btn btn-success">Return</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div> 
@endsection