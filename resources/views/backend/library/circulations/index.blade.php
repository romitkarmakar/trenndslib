@extends('layouts.backend')
@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-header">
				<strong class="card-title float-left">Circulations</strong>
				<a href="{{ url('library/circulations') }}" class="btn btn-primary btn-sm float-right">
					<i class="fa fa-reply"></i>
				</a>
			</div>
			<div class="card-body">
				<form class="params-panel" action="{{ url('library/circulations/search') }}" method="post">
					@csrf
					<div class="col-md-6 offset-md-2">
						<div class="form-group">
							<select class="form-control select2" name="member_id">
								{{ create_member_option('member_id') }}
							</select>
						</div>
					</div>
					<div class="col-md-2 text-right">
						<button type="submit" class="btn btn-success btn-block">Search</button>
					</div>
				</form>
			</div>
			<div class="text-center">
				<h4>Circulations Of</h4>
				<h4>Member Id - {{ $member->member_id }}</h4>
				<h4>Member Name - {{ $member->user->name }}</h4>
			</div>
			<div class="card-body">
				<table class="table table-striped table-bordered data-table" style="width:100%">
					<thead>
						<tr>	
							<th>Book ID</th>
							<th>Issue date</th>
							<th>Due Date</th>
							<th>Return Date</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						@php $i = 1; @endphp
						@foreach ($circulations as $data)
						<tr>
							<td>{{ $data->book->book_id }}</td>
							<td>{{ $data->issue_date }}</td>
							<td>{{ $data->due_date }}</td>
							<td>{{ $data->return_date }}</td>
							<td>
								@if($data->is_return == 1)
								<span class="badge badge-primary">Return</span>
								@elseif($data->is_return ==0)
								<span class="badge badge-danger">Not Return</span>
								@endif
							</td>
							<td class="text-center">
								<form action="{{ route('circulations.destroy',$data->id) }}" method="post">
									<a href="{{ route('circulations.show',$data->id) }}" class="btn btn-success btn-custom ajax-modal" data-title="Details"><i class="fas fa-eye" style="color: #fff" aria-hidden="true"></i></a>
									<a href="{{ route('circulations.edit',$data->id) }}" class="btn btn-warning btn-custom ajax-modal" data-title="Edit"><i class="fas fa-pencil-alt" style="color: #fff" aria-hidden="true"></i></a>
									@if($data->is_return == 0)
									<a href="{{ url('library/circulations/book_return_info/' . $data->id) }}" class="btn btn-primary btn-custom ajax-modal" data-title="Book Return Information"><i class="fas fa-reply" style="color: #fff" aria-hidden="true"></i></a>
									@endif
									<hr>
									<a href="{{ route('notifications.create', $data->id) }}" class="btn btn-info btn-custom ajax-modal" data-title="Edit"><i class="fas fa-envelope" style="color: #fff" aria-hidden="true"></i></a>
									@method('DELETE')
									@csrf
									<button type="submit" class="btn btn-danger btn-custom btn-remove"><i class="fas fa-eraser" aria-hidden="true"></i></button>
								</form>
							</td>
						</tr>
						@php $i++; @endphp
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection

@section('js-script')
<script type="text/javascript">
	$('select[name=member_id]').val('{{ $member_id }}');
</script>
@stop