<form action="{{route('circulations.store')}}" class="form-horizontal form-groups-bordered ajax-submit" enctype="multipart/form-data" method="post" accept-charset="utf-8">
	@csrf
	<div class="col-md-12">
		<div class="form-group">
			<label class="form-control-label">Member Id</label>
			<select class="form-control select2" name="member_id" required>
				{{ create_member_option('id') }}
			</select>
		</div>
	</div>
	<div class="col-md-12">
		<div class="form-group">
			<label class="form-control-label">Book Id</label>
			<select class="form-control select2" name="book_id" required>
				{{ create_option('books', 'id', 'book_id', old('book_id')) }}
			</select>
		</div>
	</div>
	<div class="col-md-12">
		<div class="form-group">
			<label class="form-control-label">Issue Date</label>
			<input type="text" name="issue_date" class="form-control datepicker" value="{{ (old('issue_date') != '') ? old('issue_date') : \Carbon\Carbon::now()->toDateString() }}" readonly required>
		</div>
	</div>
	<div class="col-md-12">
		<div class="form-group">
			<label class="form-control-label">Due Date</label>
			<input type="text" name="due_date" class="form-control datepicker" value="{{ old('due_date') }}" readonly required>
		</div>
	</div>
	<div class="col-md-12">
		<div class="form-group">
			<label class="form-control-label">Note</label>
			<textarea class="form-control" name="note" >{{ old('note') }}</textarea>
		</div>
	</div>
	<div class="col-md-12 text-right">
		<button type="submit" class="btn btn-success">Save</button>
	</div>
</form>
<script type="text/javascript">
	$('select[name=member_id]').val('{{ old('member_id') }}');
	$('select[name=book_id]').val('{{ old('book_id') }}');
</script>