<div class="library-card" id="library-card">
	<div class="card-header">
		<h5>{{ app_data('library_name') }}</h5>
		<p>Library Card</p>
	</div>
	<div class="card-image">
		<img src="{{ asset('public/uploads/images/' . $member->user->image) }}">
	</div>
	<div class="card-content">
		<table>
			<tbody>
				<tr>
					<td class="lbl">Name</td>
				</tr>
				<tr>
					<td>{{ $member->user->name }}</td>
				</tr>
				<tr>
					<td class="lbl">Member Id</td>
				</tr>	
				<tr>
					<td>{{ $member->member_id }}</td>
				</tr>
				<tr>
					<td class="lbl">Member Type</td>
				</tr>	
				<tr>
					<td>{{ $member->member_type }}</td>
				</tr>
			</tbody>
		</table>
	</div>
	<div class="card-footer">	
		<p>Emergency Contact : {{ app_data('phone') }}</p>
	</div>
</div>
<div class="col-md-12 text-right" style="margin-top: 30px;">
	<button type="submit" class="btn btn-success btn-sm print" data-print="library-card">Print</button>
</div>