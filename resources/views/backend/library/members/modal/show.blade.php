<div class="table-responsive">
	<table class="table table-bordered table-view">
		<tr class="text-center">
			<td colspan="2"><img src="{{  asset('uploads/images/' . $member->user->image) }}"></td>
		</tr>
		<tr>
			<th>Name</th>
			<td>{{ $member->user->name }}</td>
		</tr>
		<tr>
			<th>Id</th>
			<td>{{ $member->member_id }}</td>
		</tr>
		<tr>
			<th>Phone</th>
			<td>{{ $member->user->phone }}</td>
		</tr>
		<tr>
			<th>Email</th>
			<td>{{ $member->user->email }}</td>
		</tr>
		<tr>
			<th>Member Type</th>
			<td>{{ $member->member_type }}</td>
		</tr>
		<tr>
			<th>Address</th>
			<td>{{ $member->user->address }}</td>
		</tr>
	</table>
</div>
