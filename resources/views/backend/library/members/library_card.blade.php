@extends('layouts.backend')
@section('content')
<div class="row">
	<div class="col-md-8">
		<div class="card">
			<div class="card-header with-border">
				<strong class="card-title float-left">Library Card</strong>
				<a href="{{ route('members.index') }}" style="color: #000;" class="float-right">
					<i class="fas fa-reply"></i>
				</a>
			</div>
			<div class="card-body">
				<div class="library-card" id="library-card">
					<div class="card-header">
						<h5>{{ app_data('library_name') }}</h5>
						<p>Library Card</p>
					</div>
					<div class="card-image">
						<img src="{{ asset('public/uploads/images/' . $member->user->image) }}">
					</div>
					<div class="card-content">
						<table>
							<tbody>
								<tr>
									<td class="lbl">Name</td>
								</tr>
								<tr>
									<td>{{ $member->user->name }}</td>
								</tr>
								<tr>
									<td class="lbl">Member Id</td>
								</tr>	
								<tr>
									<td>{{ $member->member_id }}</td>
								</tr>
								<tr>
									<td class="lbl">Member Type</td>
								</tr>	
								<tr>
									<td>{{ $member->member_type }}</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="card-footer">	
						<p>Emergency Contact : {{ app_data('phone') }}</p>
					</div>
				</div>
				<div class="col-md-12 text-right" style="margin-top: 30px;">
					<button type="submit" class="btn btn-success btn-sm print" data-print="library-card">Print</button>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
