<form action="{{route('my_book_requests.store')}}" class="form-horizontal form-groups-bordered ajax-submit" enctype="multipart/form-data" method="post" accept-charset="utf-8">
	@csrf
	<div class="col-md-6">
		<div class="form-group">
			<label class="form-control-label">Book Title</label>
			<input type="text" name="book_title" class="form-control" value="{{ old('book_title') }}" required>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label class="form-control-label">Author Name</label>
			<input type="text" name="author_name" class="form-control" value="{{ old('author_name') }}" required>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label class="form-control-label">Edition</label>
			<input type="text" name="edition" class="form-control" value="{{ old('edition') }}" required>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label class="form-control-label">Year</label>
			<input type="text" name="year" class="form-control yearpicker" value="{{ old('year') }}" readonly required>
		</div>
	</div>
	<div class="col-md-12">
		<div class="form-group">
			<label class="form-control-label">Note</label>
			<textarea class="form-control" name="note">{{ old('note') }}</textarea>
		</div>
	</div>
	<div class="col-md-12 text-right">
		<button type="submit" class="btn btn-success">Save</button>
	</div>
</form>