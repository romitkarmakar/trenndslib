<div class="table-responsive">
	<table class="table table-bordered table-view">
		<tr>
			<th>Book Title</th>
			<td>{{ $book_request->book_title }}</td>
		</tr>
		<tr>
			<th>Author Name</th>
			<td>{{ $book_request->author_name }}</td>
		</tr>
		<tr>
			<th>Edition</th>
			<td>{{ $book_request->edition }}</td>
		</tr>
		<tr>
			<th>Year</th>
			<td>{{ $book_request->year }}</td>
		</tr>
		<tr>
			<th>Requested</th>
			<td>{{ date_format($book_request->created_at,"Y-m-d") }}</td>
		</tr>
		<tr>
			<th>Note</th>
			<td>{{ $book_request->note }}</td>
		</tr>
		<tr>
			<th>Status</th>
			<td>
				@if($book_request->status == 0)
				<span class="badge badge-warning">Pending</span>
				@elseif($book_request->status == 1)
				<span class="badge badge-success">Accepted</span>
				@elseif($book_request->status ==2)
				<span class="badge badge-danger">Rejected</span>
				@endif
			</td>
		</tr>
	</table>
</div>