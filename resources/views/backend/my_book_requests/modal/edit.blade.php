<form action="{{ route('my_book_requests.update',$book_request->id) }}" class="form-horizontal form-groups-bordered ajax-submit" enctype="multipart/form-data" method="post" accept-charset="utf-8">
	@csrf
	@method('PUT')
	<div class="col-md-6">
		<div class="form-group">
			<label class="form-control-label">Book Title</label>
			<input type="text" name="book_title" class="form-control" value="{{ $book_request->book_title }}" required>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label class="form-control-label">Author Name</label>
			<input type="text" name="author_name" class="form-control" value="{{ $book_request->author_name }}" required>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label class="form-control-label">Edition</label>
			<input type="text" name="edition" class="form-control" value="{{ $book_request->edition }}" required>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label class="form-control-label">Year</label>
			<input type="text" name="year" class="form-control yearpicker" value="{{ $book_request->year }}" readonly required>
		</div>
	</div>
	<div class="col-md-12">
		<div class="form-group">
			<label class="form-control-label">Note</label>
			<textarea class="form-control" name="note">{{ $book_request->note }}</textarea>
		</div>
	</div>
	<div class="col-md-12 text-right">
		<button type="submit" class="btn btn-success">Update</button>
	</div>
</form>