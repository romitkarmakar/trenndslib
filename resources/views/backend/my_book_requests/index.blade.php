@extends('layouts.backend')
@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-header">
				<strong class="card-title float-left">Book Requests List</strong>
				<a href="{{ route('my_book_requests.create') }}" class="btn btn-primary btn-sm float-right ajax-modal" data-title="Create">
					<i class="fa fa-plus"></i> Add New
				</a>
			</div>
			<div class="card-body">
				<table class="table table-striped table-bordered data-table" style="width:100%">
					<thead>
						<tr>
							<th>Book Title</th>
							<th>Year</th>
							<th>Requested</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						@php $i = 1; @endphp
						@foreach ($book_requests as $data)
						<tr>
							<td>{{ $data->book_title }}</td>
							<td>{{ $data->year }}</td>
							<td>{{ date_format($data->created_at,"Y-m-d") }}</td>
							<td>
								@if($data->status == 0)
								<span class="badge badge-warning">Pending</span>
								@elseif($data->status == 1)
								<span class="badge badge-success">Accepted</span>
								@elseif($data->status == 2)
								<span class="badge badge-danger">Rejected</span>
								@endif
							</td>
							<td>
								<a href="{{ route('my_book_requests.show',$data->id) }}" class="btn btn-success btn-sm  ajax-modal" data-title="Details"><i class="fa fa-eye" style="color: #fff" aria-hidden="true"></i></a>
								<a href="{{ route('my_book_requests.edit',$data->id) }}" class="btn btn-warning btn-sm  ajax-modal" data-title="Edit"><i class="fa fa-pencil-alt" style="color: #fff" aria-hidden="true"></i></a>
							</td>
						</tr>
						@php $i++; @endphp
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection
