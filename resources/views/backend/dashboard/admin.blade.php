@extends('layouts.backend')

@section('content')
<div class="row m-t-25">
	<div class="col-sm-6 col-lg-3">
		<div class="overview-item overview-item--c1">
			<div class="overview__inner">
				<div class="overview-box clearfix">
					<div class="icon">
						<i class="fas fa-users"></i>
					</div>
					<div class="text">
						<h3>{{ counter('members') }}</h3>
						<span>Total Members</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-sm-6 col-lg-3">
		<div class="overview-item overview-item--c2">
			<div class="overview__inner">
				<div class="overview-box clearfix">
					<div class="icon">
						<i class="fas fa-book"></i>
					</div>
					<div class="text">
						<h3>{{ counter('books') }}</h3>
						<span>Total Books</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-sm-6 col-lg-3">
		<div class="overview-item overview-item--c3">
			<div class="overview__inner">
				<div class="overview-box clearfix">
					<div class="icon">
						<i class="fas fa-clock"></i>
					</div>
					<div class="text">
						<h3>{{ counter('circulations') }}</h3>
						<span>Issue Books</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-sm-6 col-lg-3">
		<div class="overview-item overview-item--c4">
			<div class="overview__inner">
				<div class="overview-box clearfix">
					<div class="icon">
						<i class="fas fa-reply"></i>
					</div>
					<div class="text">
						<h3>{{ counter('circulations', ['is_return' => 1]) }}</h3>
						<span>Return Books</span>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="card">
	<div class="card-header">
		<strong class="card-title float-left">Recents Circulations</strong>
		<a href="{{ route('circulations.create') }}" class="btn btn-primary btn-sm float-right ajax-modal" data-title="Add Issue">
			<i class="fa fa-plus"></i> Add Issue
		</a>
	</div>
	<div class="card-body">
		<table class="table table-striped table-bordered data-table" style="width:100%">
			<thead>
				<tr>
					<th>Member Id</th>	
					<th>Book ID</th>
					<th>Issue date</th>
					<th>Due Date</th>
					<th>Return Date</th>
					<th>Status</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				@php $i = 1; @endphp
				@foreach ($circulations as $data)
				<tr>
					<td>{{ $data->member->member_id }}</td>
					<td>{{ $data->book->book_id }}</td>
					<td>{{ $data->issue_date }}</td>
					<td>{{ $data->due_date }}</td>
					<td>{{ $data->return_date }}</td>
					<td>
						@if($data->is_return == 1)
						<span class="badge badge-primary">Returned</span>
						@elseif($data->is_return ==0)
						<span class="badge badge-danger">Not Return</span>
						@endif
					</td>
					<td style="text-align: center;">
						<form action="{{ route('circulations.destroy',$data->id) }}" method="post">
							<a href="{{ route('circulations.show',$data->id) }}" class="btn btn-success btn-custom ajax-modal" data-title="Details"><i class="fas fa-eye" style="color: #fff" aria-hidden="true"></i></a>
							<a href="{{ route('circulations.edit',$data->id) }}" class="btn btn-warning btn-custom ajax-modal" data-title="Edit"><i class="fas fa-pencil-alt" style="color: #fff" aria-hidden="true"></i></a>
							@if($data->is_return == 0)
							<a href="{{ url('library/circulations/book_return_info/' . $data->id) }}" class="btn btn-primary btn-custom ajax-modal" data-title="Book Return Information"><i class="fas fa-reply" style="color: #fff" aria-hidden="true"></i></a>
							@endif
							<hr>
							<a href="{{ url('notifications/create/' . $data->id) }}" class="btn btn-info btn-custom ajax-modal" data-title="Edit"><i class="fas fa-envelope" style="color: #fff" aria-hidden="true"></i></a>
							@method('DELETE')
							@csrf
							<button type="submit" class="btn btn-danger btn-custom btn-remove"><i class="fas fa-eraser" aria-hidden="true"></i></button>
						</form>
					</td>
				</tr>
				@php $i++; @endphp
				@endforeach
			</tbody>
		</table>
	</div>
</div>
@endsection