@extends('layouts.backend')

@section('content')
<div class="row m-t-25">
	<div class="col-sm-6 col-lg-3">
		<div class="overview-item overview-item--c1">
			<div class="overview__inner">
				<div class="overview-box clearfix">
					<div class="icon">
						<i class="fas fa-book"></i>
					</div>
					<div class="text">
						<h3>{{ counter('books') }}</h3>
						<span>Total Books</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-sm-6 col-lg-3">
		<div class="overview-item overview-item--c2">
			<div class="overview__inner">
				<div class="overview-box clearfix">
					<div class="icon">
						<i class="fas fa-clock"></i>
					</div>
					<div class="text">
						<h3>{{ counter('circulations', ['member_id' => get_member_id()]) }}</h3>
						<span>Issue Books</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-sm-6 col-lg-3">
		<div class="overview-item overview-item--c3">
			<div class="overview__inner">
				<div class="overview-box clearfix">
					<div class="icon">
						<i class="fas fa-arrow-right"></i>
					</div>
					<div class="text">
						<h3>{{ counter('circulations', ['member_id' => get_member_id(), 'is_return' => 0]) }}</h3>
						<span>Due Books</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-sm-6 col-lg-3">
		<div class="overview-item overview-item--c4">
			<div class="overview__inner">
				<div class="overview-box clearfix">
					<div class="icon">
						<i class="fas fa-reply"></i>
					</div>
					<div class="text">
						<h3>{{ counter('circulations', ['member_id' => get_member_id(), 'is_return' => 1]) }}</h3>
						<span>Return Books</span>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="card">
	<div class="card-header">
		<strong class="card-title float-left">Recents Circulations</strong>
	</div>
	<div class="card-body">
		<table class="table table-striped table-bordered data-table" style="width:100%">
			<thead>
				<tr>	
					<th>Book ID</th>
					<th>Issue date</th>
					<th>Due Date</th>
					<th>Return Date</th>
					<th>Status</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				@php $i = 1; @endphp
				@foreach ($circulations as $data)
				<tr>
					<td>{{ $data->book->book_id }}</td>
					<td>{{ $data->issue_date }}</td>
					<td>{{ $data->due_date }}</td>
					<td>{{ $data->return_date }}</td>
					<td>
						@if($data->is_return == 1)
						<span class="badge badge-primary">Returned</span>
						@elseif($data->is_return ==0)
						<span class="badge badge-danger">Not Return</span>
						@endif
					</td>
					<td>
						<a href="{{ route('my_circulations.show',$data->id) }}" class="btn btn-success btn-sm  ajax-modal" data-title="Details"><i class="fa fa-eye" style="color: #fff" aria-hidden="true"></i></a>
					</td>
				</tr>
				@php $i++; @endphp
				@endforeach
			</tbody>
		</table>
	</div>
</div>
@endsection