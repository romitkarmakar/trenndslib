<form action="{{ url('profile/update') }}" class="form-horizontal form-groups-bordered ajax-submit" enctype="multipart/form-data" method="post" accept-charset="utf-8">
	@csrf
	<div class="col-md-12">
		<div class="form-group">
			<label class="form-control-label">Name</label>
			<input type="text" name="name" class="form-control" value="{{ $user->name }}" required>
		</div>
	</div>
	<div class="col-md-12">
		<div class="form-group">
			<label class="form-control-label">Phone</label>
			<input type="text" name="phone" class="form-control" value="{{ $user->phone }}">
		</div>
	</div>
	<div class="col-md-12">
		<div class="form-group">
			<label class="form-control-label">Email</label>
			<input type="text" name="email" class="form-control" value="{{ $user->email }}" required>
		</div>
	</div>
	<div class="col-md-12">
		<div class="form-group">
			<label class="form-control-label">Address </label>
			<textarea class="form-control" name="address" required>{{ $user->address }}</textarea>
		</div>
	</div>
	<div class="col-md-12">
		<div class="form-group">
			<label class="control-label">Image</label>						
			<input type="file" class="form-control dropify" name="image" data-default-file="{{ ($user->image != '') ? asset('public/uploads/images/' . $user->image) : '' }}">
		</div>
	</div>
	<div class="col-md-12 text-right">
		<button type="submit" class="btn btn-success">Update</button>
	</div>
</form>
