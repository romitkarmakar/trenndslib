@extends('layouts.backend')
@section('content')
<div class="row">
	<div class="col-md-6">
		<div class="card">
			<div class="card-header with-border">
				<strong class="card-title float-left">Change Password</strong>
			</div>
			<div class="card-body">
				<form action="{{ url('password/update') }}" class="form-horizontal form-groups-bordered validate" enctype="multipart/form-data" method="post" accept-charset="utf-8">
					@csrf
					<div class="col-md-12">
						<div class="form-group">
							<label class="form-control-label">Old Password</label>
							<input type="password" name="oldpassword" class="form-control" value="{{ old('oldpassword') }}"required>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label class="form-control-label">Password</label>
							<input type="password" name="password" class="form-control" value="{{ old('password') }}" required>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label class="form-control-label">Password Confirmation</label>
							<input type="password" name="password_confirmation" class="form-control" value="{{ old('password_confirmation') }}" required>
						</div>
					</div>
					<div class="col-md-12 text-right">
						<button type="submit" class="btn btn-success">Change</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection
