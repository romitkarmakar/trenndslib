@extends('layouts.backend')
@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-header">
				<h4>Notifications</h4>
			</div>
			<div class="card-body">
				<table class="table table-striped table-bordered data-table" style="width:100%">
					<thead>
						<tr>	
							<th>Subject</th>
							<th>Time</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						@php $i = 1; @endphp
						@foreach ($notifications as $data)
						<tr>
							<td>{{ $data->subject }}</td>
							<td>{{ $data->created_at }}</td>
							<td>
								@if($data->status == 0)
									<span class="badge badge-primary">Unread</span>
								@elseif($data->status == 1)
									<span class="badge badge-info">Readed</span>
								@endif
							</td>
							<td>
								<a href="{{ url('my_notifications/' . $data->id) }}" class="btn btn-success btn-sm"><i class="fa fa-eye" style="color: #fff" aria-hidden="true"></i></a>
							</td>
						</tr>
						@php $i++; @endphp
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection