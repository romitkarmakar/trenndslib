@extends('layouts.backend')
@section('content')
<div class="row">
	<div class="col-md-8">
		<div class="card">
			<div class="card-header">
				<strong class="card-title float-left">Details</strong>
				<a href="{{ url('my_notifications') }}" style="color: #000;" class="float-right">
					<i class="fas fa-reply"></i>
				</a>
			</div>
			<div class="card-body">
				<div class="table-responsive">
					<table class="table table-bordered table-view">
						<tr>
							<th>Subject</th>
							<td>{{ $notification->subject }}</td>
						</tr>
						<tr>
							<th>Message</th>
							<td>{!! get_message($notification->id) !!}</td>
						</tr>
						<tr>
							<th>Sended At</th>
							<td>{{ $notification->created_at }}</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
