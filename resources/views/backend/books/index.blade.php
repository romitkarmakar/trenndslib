@extends('layouts.backend')
@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-header">
				<strong class="card-title float-left">List</strong>
			</div>
			<div class="card-body">
				<table class="table table-striped table-bordered data-table" style="width:100%">
					<thead>
						<tr>
							<th>Photo</th>	
							<th>Title</th>
							<th>Category</th>
							<th>Book Id</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						@php $i = 1; @endphp
						@foreach ($books as $data)
						<tr>
							<td><img class="profile" src="{{  asset('public/uploads/images/' . $data->photo) }}"></td>
							<td>{{ $data->title }}</td>
							<td>{{ $data->category->category_name }}</td>
							<td>{{ $data->book_id }}</td>
							<td>
								<a href="{{ url('books/' . $data->id) }}" class="btn btn-success btn-custom  ajax-modal" data-title="Details"><i class="fa fa-eye" style="color: #fff" aria-hidden="true"></i></a>
							</td>
						</tr>
						@php $i++; @endphp
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection
