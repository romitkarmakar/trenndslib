@extends('layouts.backend')
@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-header">
				<strong class="card-title float-left">Circulations List</strong>
			</div>
			<div class="card-body">
				<table class="table table-striped table-bordered data-table" style="width:100%">
					<thead>
						<tr>	
							<th>Book ID</th>
							<th>Issue date</th>
							<th>Due Date</th>
							<th>Return Date</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						@php $i = 1; @endphp
						@foreach ($circulations as $data)
						<tr>
							<td>{{ $data->book->book_id }}</td>
							<td>{{ $data->issue_date }}</td>
							<td>{{ $data->due_date }}</td>
							<td>{{ $data->return_date }}</td>
							<td>
								@if($data->is_return == 1)
								<span class="badge badge-primary">Return</span>
								@elseif($data->is_return ==0)
								<span class="badge badge-danger">Not Return</span>
								@endif
							</td>
							<td>
								<a href="{{ route('my_circulations.show',$data->id) }}" class="btn btn-success btn-sm  ajax-modal" data-title="Details"><i class="fa fa-eye" style="color: #fff" aria-hidden="true"></i></a>
							</td>
						</tr>
						@php $i++; @endphp
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection
