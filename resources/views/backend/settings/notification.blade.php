@extends('layouts.backend')
@section('content')
<div class="row">
	<div class="col-md-8">
		<div class="card">
			<div class="card-header with-border">
				<strong class="card-title float-left">Notification Settings</strong>
			</div>
			<div class="card-body">
				<form action="{{ url('notification_settings') }}" class="form-horizontal form-groups-bordered validate" method="post" accept-charset="utf-8">
					@csrf
					<div class="col-md-12">
						<div class="alert alert-danger">
							<strong>CronJob Url - </strong> http://{app_url}/automatic_notify
						</div>
						<div class="form-group">
							<label class="form-control-label">Automatic Notify Delayed Members</label>
							<select class="form-control select2" name="auto_notify_delayed_members" required>
								<option value="1">Yes</option>
								<option value="0">No</option>
							</select>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label class="form-control-label">Notification Type</label>
							<select class="form-control select2" name="notification_type" required>
								<option value="1">Only Notification</option>
								<option value="2">Only Email</option>
								<option value="3">Notification And Email</option>
							</select>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label class="form-control-label">Notification Subject</label>
							<input type="text" name="notification_subject" class="form-control" value="{{ app_data('notification_subject') }}" required>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label class="form-control-label">Notification Message</label>
							<textarea class="form-control" name="notification_message" rows="6" required>{{ app_data('notification_message') }}</textarea>
						</div>
					</div>
					<div class="col-md-12 text-right">
						<button type="submit" class="btn btn-success">Save</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="alert alert-info">
			<strong>Notice!</strong></br> You can also use those dynamic codes in message.</br>
			1. $member_name</br>
			2. $member_id</br>
			3. $book_title</br>
			4. $book_id</br>
			5. $issue_date</br>
			6. $due_date</br>
			Example - hello $member_name output will hello john doe(member name)
		</div>
	</div>
</div> 
@endsection

@section('js-script')
<script type="text/javascript">
	$('select[name=auto_notify_delayed_members]').val('{{ app_data('auto_notify_delayed_members') }}');
	$('select[name=notification_type]').val('{{ app_data('notification_type') }}');
	$(document).ready(function (){
		var auto_notify_status = '{{ app_data('auto_notify_delayed_members') }}';
		if(auto_notify_status == 0){
			$('.alert-danger').hide();
		}
		$('select[name=auto_notify_delayed_members]').on('change', function(){
			var auto_notify_status = $('select[name=auto_notify_delayed_members]').val();
			if(auto_notify_status == 0){
				$('.alert-danger').fadeOut( "slow" );
			}else{
				$('.alert-danger').fadeIn( "slow" );
			}
		});
	});
</script>
@stop