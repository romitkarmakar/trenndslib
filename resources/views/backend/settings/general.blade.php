@extends('layouts.backend')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-header">
				<strong class="card-title">General Settings</strong>
			</div>
			<div class="card-body">
				<div class="default-tab tabs-left">
					<nav>
						<div class="nav nav-tabs nav-justified" id="nav-tab" role="tablist">
							<a class="nav-item nav-link active" data-toggle="tab" href="#information" role="tab" aria-selected="true">Information</a>
							<a class="nav-item nav-link" data-toggle="tab" href="#email" role="tab" aria-selected="true">Email</a>
							<a class="nav-item nav-link" data-toggle="tab" href="#logo" role="tab" aria-selected="false">Logo</a>
						</div>
					</nav>
					<div class="tab-content pt-2" id="nav-tabContent">
						<div class="tab-pane fade show active" id="information" role="tabpanel" aria-labelledby="nav-home-tab">
							<div class="card">
								<div class="card-body">
									<form action="{{ url('general_settings') }}" class="form-horizontal form-groups-bordered validate" enctype="multipart/form-data" method="post" accept-charset="utf-8">
										@csrf

										<div class="col-md-12">
											<div class="form-group">
												<label class="form-control-label">Library Name</label>
												<input type="text" name="library_name" class="form-control" value="{{ app_data('library_name') }}" required>
											</div>
										</div>
										<div class="col-md-12">
											<div class="form-group">
												<label class="form-control-label">Phone</label>
												<input type="text" name="phone" class="form-control" value="{{ app_data('phone') }}" required>
											</div>
										</div>
										<div class="col-md-12">
											<div class="form-group">
												<label class="form-control-label">Address</label>
												<textarea class="form-control" name="address" required>{{ app_data('address') }}</textarea>
											</div>
										</div>

										<div class="col-md-12">
											<div class="form-group">
												<label class="form-control-label">Copyright</label>
												<input type="text" name="copy_right" class="form-control" value="{{ app_data('copy_right') }}" required>
											</div>
										</div>
										<div class="col-md-12">
											<div class="form-group">
												<label class="control-label">Timezone</label>	
												<select class="form-control" name="timezone" required>
													<option value="">Select One</option>
													{{ create_timezone_option(app_data('timezone')) }}
												</select>
											</div>
										</div>
										<div class="col-md-12 text-right">
											<button type="submit" class="btn btn-success">Save</button>
										</div>
									</form>
								</div>
							</div>
						</div>
						<div class="tab-pane fade" id="email" role="tabpanel" aria-labelledby="nav-profile-tab">
							<div class="card">
								<div class="card-body">
									<form action="{{ url('general_settings') }}" class="form-horizontal form-groups-bordered validate" enctype="multipart/form-data" method="post" accept-charset="utf-8">
										@csrf

										<div class="col-md-12">
											<div class="form-group">
												<label class="control-label">From Email</label>
												<input type="text" class="form-control" name="from_email" value="{{ app_data('from_email') }}" required>
											</div>
										</div>

										<div class="col-md-12">
											<div class="form-group">
												<label class="control-label">From Name</label>	
												<input type="text" class="form-control" name="from_name" value="{{ app_data('from_name') }}" required>
											</div>
										</div>

										<div class="col-md-12">
											<div class="form-group">
												<label class="control-label">SMTP Host</label>		
												<input type="text" class="form-control smtp" name="smtp_host" value="{{ app_data('smtp_host') }}" required>
											</div>
										</div>

										<div class="col-md-12">
											<div class="form-group">
												<label class="control-label">SMTP Port</label>		
												<input type="text" class="form-control smtp" name="smtp_port" value="{{ app_data('smtp_port') }}" required>
											</div>
										</div>

										<div class="col-md-12">
											<div class="form-group">
												<label class="control-label">SMTP Username</label>	
												<input type="text" class="form-control smtp" autocomplete="off" name="smtp_username" value="{{ app_data('smtp_username') }}" required>
											</div>
										</div>

										<div class="col-md-12">
											<div class="form-group">
												<label class="control-label">SMTP Password</label>
												<input type="password" class="form-control smtp" autocomplete="off" name="smtp_password" value="{{ app_data('smtp_password') }}" required>
											</div>
										</div>

										<div class="col-md-12">
											<div class="form-group">
												<label class="control-label">SMTP Encryption</label>
												<select class="form-control smtp" name="smtp_encryption" required>
													<option value="ssl" {{ app_data('smtp_encryption') =="ssl" ? "selected" : "" }}>SSL</option>
													<option value="tls" {{ app_data('smtp_encryption') =="tls" ? "selected" : "" }}>TLS</option>
												</select>
											</div>
										</div>

										<div class="col-md-12 text-right">
											<button type="submit" class="btn btn-success">Save</button>
										</div>
									</form>
								</div>
							</div>
						</div>
						<div class="tab-pane fade" id="logo" role="tabpanel" aria-labelledby="nav-profile-tab">
							<form action="{{ url('general_settings') }}" class="form-horizontal form-groups-bordered validate" enctype="multipart/form-data" method="post" accept-charset="utf-8">
								@csrf
								<div class="card">
									<div class="card-body">
										<div class="col-md-12">
											<div class="form-group">
												<label class="control-label">Logo (<span style="color: #ff0000">175 X 50 px</span>)</label>				
												<input type="file" class="form-control dropify" name="logo" data-default-file="{{ (app_data('logo') != '') ? asset('public/uploads/images/' . app_data('logo')) : '' }}">
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-12 text-right">
									<button type="submit" class="btn btn-success">Save</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>  
</div>
@endsection

