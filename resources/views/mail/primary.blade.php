@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        @component('mail::header', ['url' => '#'])
            <img src="{{ asset('uploads/images/' . app_data('logo')) }}" alt="{{ app_data('library_name') }}" />
        @endcomponent
    @endslot

	{{-- Body --}}
    
	{!! $content->message !!}

	{{-- Footer --}}
    @slot('footer')
        {{-- @component('mail::footer')
            <h4 style="text-align: center;">Best regards</h4>
            {{ app_data('library_name') }}
        @endcomponent --}}
        @component('mail::footer')
            <h4 style="text-align: center;">
                Best Regards<br>
                {{ app_data('library_name') }}
            </h4>
            {{ app_data('copy_right') }}
        @endcomponent
    @endslot
@endcomponent