@extends('install.layout')

@section('content')
<div class="panel panel-default">
	<div class="panel-heading text-center">Login Details</div>
	<div class="panel-body">
		<div class="col-md-12">
			@if ($errors->any())
			<div class="alert alert-danger alert-dismissible">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				@foreach ($errors->all() as $error)
				<p>{{ $error }}</p>
				@endforeach
			</div>
			@endif
			<form action="{{ url('install/store_user') }}" method="post" autocomplete="off">
				{{ csrf_field() }}
				<div class="form-group">
					<label class="form-control-label">Name</label>
					<input type="text" name="name" class="form-control" value="{{ old('name') }}" required>
				</div>
				<div class="form-group">
					<label class="form-control-label">Phone</label>
					<input type="text" name="phone" class="form-control" value="{{ old('phone') }}">
				</div>
				<div class="form-group">
					<label class="form-control-label">Email</label>
					<input type="text" name="email" class="form-control" value="{{ old('email') }}" required>
				</div>
				<div class="form-group">
					<label class="form-control-label">Address </label>
					<textarea class="form-control" name="address" required>{{ old('address') }}</textarea>
				</div>
				<div class="form-group">
					<label class="form-control-label">Password</label>
					<input type="password" name="password" class="form-control" value="{{ old('password') }}" required>
				</div>
				<div class="form-group">
					<label class="form-control-label">Password Confirmation</label>
					<input type="password" name="password_confirmation" class="form-control" value="{{ old('password_confirmation') }}" required>
				</div>
				
				<button type="submit" id="next-button" class="btn btn-install">Next</button>
			</form>
		</div>
	</div>
</div>
@endsection
