@extends('install.layout')

@section('content')
<div class="panel panel-default">
	<div class="panel-heading text-center">System Settings</div>
	<div class="panel-body">
		<form action="{{ url('install/finish') }}" method="post" autocomplete="off">
			{{ csrf_field() }}
				<div class="form-group">
					<label class="form-control-label">Library Name</label>
					<input type="text" name="library_name" class="form-control" value="{{ app_data('library_name') }}" required>
				</div>
				<div class="form-group">
					<label class="form-control-label">Phone</label>
					<input type="text" name="phone" class="form-control" value="{{ app_data('phone') }}" required>
				</div>
				<div class="form-group">
					<label class="form-control-label">Address</label>
					<textarea class="form-control" name="address" required>{{ app_data('address') }}</textarea>
				</div>
				<div class="form-group">
					<label class="form-control-label">Copyright</label>
					<input type="text" name="copy_right" class="form-control" value="{{ app_data('copy_right') }}" required>
				</div>
				<div class="form-group">
					<label class="control-label">Timezone</label>	
					<select class="form-control" name="timezone" required>
						<option value="">Select One</option>
						{{ create_timezone_option() }}
					</select>
				</div>
				<button type="submit" id="next-button" class="btn btn-install">Finish</button>
		</form>
	</div>
</div>
@endsection

