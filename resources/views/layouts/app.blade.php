<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Title Page-->
    <title>{{ app_data('library_name', 'Login') }}</title>
    <!-- Fontfaces CSS-->
    <link href="{{ asset('public/login-assets') }}/css/font-face.css" rel="stylesheet" media="all">
    <link href="{{ asset('public/login-assets') }}/vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="{{ asset('public/login-assets') }}/vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="{{ asset('public/login-assets') }}/vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
    <!-- Bootstrap CSS-->
    <link href="{{ asset('public/login-assets') }}/vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">
    <!-- Main CSS-->
    <link href="{{ asset('public/login-assets') }}/css/theme.css" rel="stylesheet" media="all">
</head>
<body class="animsition">
    <div class="page-wrapper">
        <div class="page-content--bge5">
            <div class="container">
                <div class="login-wrap">
                    <div class="login-content">
                        <div class="login-logo">
                            <a href="{{ url('/login') }}">
                                <img src="{{ asset('public/uploads/images/' . app_data('logo')) }}" alt="{{ app_data('library_name', 'Logo') }}">
                            </a>
                        </div>
                        <div class="login-form">
                            
                            @yield('content')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Jquery JS-->
    <script src="{{ asset('public/login-assets') }}/vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="{{ asset('public/login-assets') }}/vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="{{ asset('public/login-assets') }}/vendor/bootstrap-4.1/bootstrap.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function (){
            $(".alert").hide();
            $(".alert").fadeTo(2000, 500).slideUp(500, function(){
                $(".alert").slideUp(500);
            }); 
        });
    </script>
</body>
</html>
