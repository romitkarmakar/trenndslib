<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Title Page-->
    <title>{{ app_data('library_name') }}</title>

    <!-- Fontfaces CSS-->
    <link href="{{ asset('public/backend') }}/css/font-face.css" rel="stylesheet" media="all">
    <link href="{{ asset('public/backend') }}/vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="{{ asset('public/backend') }}/vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="{{ asset('public/backend') }}/vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="{{ asset('public/backend') }}/vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="{{ asset('public/backend') }}/vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="{{ asset('public/backend') }}/vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="{{ asset('public/backend') }}/vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="{{ asset('public/backend') }}/vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="{{ asset('public/backend') }}/vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="{{ asset('public/backend') }}/vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="{{ asset('public/backend') }}/vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">
    <link href="{{ asset('public/backend') }}/vendor/data-table/css/dataTables.bootstrap4.min.css" rel="stylesheet" media="all">
    <link href="{{ asset('public/backend') }}/vendor/data-table/css/responsive.bootstrap4.min.css" rel="stylesheet" media="all">
    <link href="{{ asset('public/backend') }}/css/dropify.min.css" rel="stylesheet" media="all">
    <link href="{{ asset('public/backend') }}/css/bootstrap-datepicker.css" rel="stylesheet" media="all">
    <link href="{{ asset('public/backend') }}/css/fullcalendar.min.css" rel="stylesheet" media="all">
    <link href="{{ asset('public/backend') }}/css/summernote.css" rel="stylesheet" media="all">
    <link href="{{ asset('public/backend') }}/css/toastr.css" rel="stylesheet" media="all">
    <!-- Main CSS-->
    <link href="{{ asset('public/backend') }}/css/theme.css" rel="stylesheet" media="all">

</head>

<body class="animsition">
    <div class="page-wrapper">
        <!-- HEADER MOBILE-->
        <header class="header-mobile d-block d-lg-none">
            <div class="header-mobile__bar">
                <div class="container-fluid">
                    <div class="header-mobile-inner">
                        <a class="logo" href="{{ url('/dashboard') }}">
                            <img src="{{ asset('public/uploads/images/' . app_data('logo')) }}" alt="{{ app_data('library_name', 'Logo') }}" />
                        </a>
                        <button class="hamburger hamburger--slider" type="button">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
            <nav class="navbar-mobile">
                <div class="container-fluid">
                    <ul class="navbar-mobile__list list-unstyled">
                        <li @if(Request::is('dashboard')) class="active" @endif>
                            <a href="{{url('dashboard')}}">
                                <i class="fas fa-tachometer-alt"></i>
                                Dashboard
                            </a>
                        </li>
                        <li class="has-sub @if((Request::is('general_settings'))OR(Request::is('notification_settings')))active @endif">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-user"></i>Profile
                            </a>
                            <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
                                <li>
                                    <a href="{{ url('profile/show') }}" class="ajax-modal" data-title="Profile">Profile</a>
                                </li>
                                <li>
                                    <a href="{{ url('profile/edit') }}" class="ajax-modal" data-title="Update Profile">Update Profile</a>
                                </li>
                                <li>
                                    <a href="{{ url('password/change') }}" class="ajax-modal" data-title="Change Password"></a>
                                </li>
                            </ul>
                        </li>
                        @if(Auth::user()->role != 'Member')
                        <li @if((Request::is('library'))OR(Request::is('library/*'))) class="active" @endif>
                            <a href="{{url('library')}}">
                                <i class="fa fa-book"></i>
                                Library
                            </a>
                        </li>
                        <li @if((Request::is('book_requests'))OR(Request::is('book_requests/*'))) class="active" @endif>
                            <a href="{{ url('book_requests') }}">
                                <i class="fa fa-bell"></i>
                                Book Requests
                            </a>
                        </li>
                        <li @if((Request::is('notifications'))OR(Request::is('notifications/*'))) class="active" @endif>
                            <a href="{{route('notifications.index')}}">
                                <i class="fa fa-bell"></i>
                                Notifications
                            </a>
                        </li>
                        @if(Auth::user()->role == 'Admin')
                        <li @if((Request::is('users'))OR(Request::is('users/*'))) class="active" @endif>
                            <a href="{{route('users.index')}}">
                                <i class="fa fa-users"></i>
                                Users Management
                            </a>
                        </li>
                        <li class="has-sub @if((Request::is('general_settings'))OR(Request::is('notification_settings')))active @endif">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-cogs"></i>Settings
                            </a>
                            <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
                                <li>
                                    <a href="{{ url('general_settings') }}">General</a>
                                </li>
                                <li>
                                    <a href="{{ url('notification_settings') }}">Notification</a>
                                </li>
                            </ul>
                        </li>
                        <li @if(Request::is('backup_database')) class="active" @endif>
                            <a href="{{ url('backup_database') }}" target="_blank">
                                <i class="fa fa-database"></i>
                                Database Backup
                            </a>
                        </li>
                        @endif
                        @endif
                        @if(Auth::user()->role == 'Member')
                        <li @if((Request::is('books'))OR(Request::is('books/*'))) class="active" @endif>
                            <a href="{{ url('books') }}">
                                <i class="fas fa-book"></i>
                                Books
                            </a>
                        </li>
                        <li @if((Request::is('my_circulations'))OR(Request::is('book_requests/*'))) class="active" @endif>
                            <a href="{{ url('my_circulations') }}">
                                <i class="fas fa-clock"></i>
                                Circulations
                            </a>
                        </li>
                        <li @if((Request::is('my_book_requests'))OR(Request::is('my_book_requests/*'))) class="active" @endif>
                            <a href="{{ url('my_book_requests') }}">
                                <i class="fas fa-paper-plane"></i>
                                Book Requests
                            </a>
                        </li>
                        <li @if((Request::is('my_notifications'))OR(Request::is('my_notifications/*'))) class="active" @endif>
                            <a href="{{ url('my_notifications') }}">
                                <i class="fa fa-bell"></i>
                                Notifications
                            </a>
                        </li>
                        @endif
                    </ul>
                </div>
            </nav>
        </header>
        <!-- END HEADER MOBILE-->

        <!-- MENU SIDEBAR-->
        <aside class="menu-sidebar d-none d-lg-block">
            <div class="logo">
                <a href="{{ url('dashboard') }}">
                    <img src="{{ asset('public/uploads/images/'.app_data('logo')) }}" alt="{{ app_data('library_name', 'Logo') }}" />
                </a>
            </div>
            <div class="menu-sidebar__content js-scrollbar1">
                <nav class="navbar-sidebar">
                    <ul class="list-unstyled navbar__list">
                        <li @if(Request::is('dashboard')) class="active" @endif>
                            <a href="{{ url('dashboard') }}">
                                <i class="fas fa-tachometer-alt"></i>
                                Dashboard
                            </a>
                        </li>
                        <li class="has-sub @if((Request::is('profile/*'))OR (Request::is('password/*')))active @endif">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-user"></i>Profile
                            </a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li>
                                    <a href="{{ url('profile/show') }}" class="ajax-modal" data-title="Profile">Profile</a>
                                </li>
                                <li>
                                    <a href="{{ url('profile/edit') }}" class="ajax-modal" data-title="Update Profile">Update Profile</a>
                                </li>
                                <li>
                                    <a href="{{ url('password/change') }}" class="ajax-modal" data-title="Change Password">Change Password</a>
                                </li>
                            </ul>
                        </li>
                        @if(Auth::user()->role != 'Member')
                        <li @if((Request::is('library'))OR(Request::is('library/*'))) class="active" @endif>
                            <a href="{{ url('library') }}">
                                <i class="fa fa-book"></i>
                                Library
                            </a>
                        </li>
                        <li @if((Request::is('book_requests'))OR(Request::is('book_requests/*'))) class="active" @endif>
                            <a href="{{ url('book_requests') }}">
                                <i class="fas fa-paper-plane"></i>
                                @php
                                $book_requests = counter('book_requests', ['status' => 0]);
                                @endphp
                                Book Requests @if($book_requests > 0) <span class="badge badge-success">{{ $book_requests }}</span> @endif
                            </a>
                        </li>
                        <li @if((Request::is('notifications'))OR(Request::is('notifications/*'))) class="active" @endif>
                            <a href="{{ route('notifications.index') }}">
                                <i class="fa fa-bell"></i>
                                Notifications
                            </a>
                        </li>
                        @if(Auth::user()->role == 'Admin')
                        <li @if((Request::is('users'))OR(Request::is('users/*'))) class="active" @endif>
                            <a href="{{ route('users.index') }}">
                                <i class="fa fa-users"></i>
                                Users Management
                            </a>
                        </li>
                        <li class="has-sub @if((Request::is('general_settings'))OR(Request::is('frontend_settings')))active @endif">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-cogs"></i>Settings
                            </a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li>
                                    <a href="{{ url('general_settings') }}">General</a>
                                </li>
                                <li>
                                    <a href="{{ url('notification_settings') }}">Notifications</a>
                                </li>
                            </ul>
                        </li>
                        <li @if(Request::is('backup_database')) class="active" @endif>
                            <a href="{{ url('backup_database') }}" target="_blank">
                                <i class="fa fa-database"></i>
                                Database Backup
                            </a>
                        </li>
                        @endif
                        @endif
                        @if(Auth::user()->role == 'Member')
                        <li @if((Request::is('books'))OR(Request::is('books/*'))) class="active" @endif>
                            <a href="{{ url('books') }}">
                                <i class="fas fa-book"></i>
                                Books
                            </a>
                        </li>
                        <li @if((Request::is('my_circulations'))OR(Request::is('book_requests/*'))) class="active" @endif>
                            <a href="{{ url('my_circulations') }}">
                                <i class="fas fa-clock"></i>
                                Circulations
                            </a>
                        </li>
                        <li @if((Request::is('my_book_requests'))OR(Request::is('my_book_requests/*'))) class="active" @endif>
                            <a href="{{ url('my_book_requests') }}">
                                <i class="fas fa-paper-plane"></i>
                                Book Requests
                            </a>
                        </li>
                        <li @if((Request::is('my_notifications'))OR(Request::is('my_notifications/*'))) class="active" @endif>
                            <a href="{{ url('my_notifications') }}">
                                <i class="fa fa-bell"></i>
                                Notifications
                            </a>
                        </li>
                        @endif
                    </ul>
                </nav>
            </div>
        </aside>
        <!-- END MENU SIDEBAR-->

        <!-- PAGE CONTAINER-->
        <div class="page-container">
            <!-- HEADER DESKTOP-->
            <header class="header-desktop">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="header-wrap">
                            <div class="form-header">
                                <h3>Dashboard</h3>
                            </div>
                            <div class="header-button">
                                <div class="noti-wrap">
                                    @if(Auth::user()->role == 'Member')
                                    <div class="noti__item js-item-menu">
                                        <i class="fas fa-bell"></i>
                                        {{-- define number of new emails --}}
                                        @php
                                        $new_notifications = count(new_notifications());
                                        @endphp
                                        {{-- define number of new emails --}}
                                        @if ($new_notifications > 0)
                                        <span class="quantity">{{ $new_notifications }}</span>
                                        @endif

                                        <div class="email-dropdown js-dropdown">
                                            <div class="email__title">
                                                <p>You have {{ ($new_notifications > 0) ? $new_notifications : 'No' }} New Notifications</p>
                                            </div>
                                            @foreach (new_notifications() as $data)
                                            <a href="{{ url('my_notifications/' . $data->id) }}" style="display: block;">
                                                <div class="email__item">
                                                    <div class="image img-cir img-40">
                                                        <i class="zmdi zmdi-email-open"></i>
                                                    </div>
                                                    <div class="content">
                                                        <p>{{ $data->subject }}</p>
                                                        <span>{{ time_elapsed_string($data->created_at) }}</span>
                                                    </div>
                                                </div>
                                            </a>
                                            @endforeach

                                            <div class="email__footer">
                                                <a href="{{ url('my_notifications') }}">See all Notifications</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endif
                                <div class="account-wrap">
                                    <div class="account-item clearfix js-item-menu">
                                        <div class="image">
                                            <img src="{{ asset('public/uploads/images/' . Auth::User()->image) }}" alt="Profile" />
                                        </div>
                                        <div class="content">
                                            <a class="js-acc-btn" href="#">Hi, {{ Auth::User()->name }}</a>
                                        </div>
                                        <div class="account-dropdown js-dropdown">
                                            <div class="info clearfix">
                                                <div class="image">
                                                    <a href="#">
                                                        <img src="{{ asset('public/uploads/images/' . Auth::User()->image) }}" alt="Profile" />
                                                    </a>
                                                </div>
                                                <div class="content">
                                                    <h5 class="name">
                                                        <a href="#">{{ Auth::User()->name }}</a>
                                                    </h5>
                                                    <span class="email">{{ Auth::User()->email }}</span>
                                                </div>
                                            </div>
                                            <div class="account-dropdown__body">
                                                <div class="account-dropdown__item">
                                                    <a href="{{ url('profile/edit') }}" class="ajax-modal" data-title="Edit">
                                                        <i class="zmdi zmdi-account"></i>Update Profile
                                                    </a>
                                                </div>
                                                <div class="account-dropdown__item">
                                                    <a href="{{ url('password/change') }}" class="ajax-modal" data-title="Change Password">
                                                        <i class="zmdi zmdi-settings"></i>Change Password
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="account-dropdown__footer">
                                                <a href="#" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                                    <i class="zmdi zmdi-power"></i>Logout
                                                </a>
                                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                    @csrf
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <div class="main-content">
                <div class="section__content section__content--p10">
                    <div class="container-fluid">
                        @yield('content')

                        <div class="row">
                            <div class="col-md-12">
                                <div class="copyright">
                                    {!! app_data('copy_right') !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="main_modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"></h5>
                </div>
                <div class="modal-body"></div>
            </div>
        </div>
    </div>
    <div id="loader"><div class="loader__spin"></div></div>
</body>
<!-- Jquery JS-->
<script src="{{ asset('public/backend') }}/vendor/jquery-3.2.1.min.js"></script>
<!-- Bootstrap JS-->
<script src="{{ asset('public/backend') }}/vendor/bootstrap-4.1/popper.min.js"></script>
<script src="{{ asset('public/backend') }}/vendor/bootstrap-4.1/bootstrap.min.js"></script>
<!-- Vendor JS       -->
<script src="{{ asset('public/backend') }}/vendor/slick/slick.min.js"></script>
<script src="{{ asset('public/backend') }}/vendor/wow/wow.min.js"></script>
<script src="{{ asset('public/backend') }}/vendor/animsition/animsition.min.js"></script>
<script src="{{ asset('public/backend') }}/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
<script src="{{ asset('public/backend') }}/vendor/counter-up/jquery.waypoints.min.js"></script>
<script src="{{ asset('public/backend') }}/vendor/counter-up/jquery.counterup.min.js"></script>
<script src="{{ asset('public/backend') }}/vendor/circle-progress/circle-progress.min.js"></script>
<script src="{{ asset('public/backend') }}/vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
<script src="{{ asset('public/backend') }}/vendor/chartjs/Chart.bundle.min.js"></script>
<script src="{{ asset('public/backend') }}/vendor/select2/select2.min.js"></script>
<script src="{{ asset('public/backend') }}/vendor/data-table/js/jquery.dataTables.min.js"></script>
<script src="{{ asset('public/backend') }}/vendor/data-table/js/dataTables.bootstrap4.min.js"></script>
<script src="{{ asset('public/backend') }}/vendor/data-table/js/dataTables.responsive.min.js"></script>
<script src="{{ asset('public/backend') }}/vendor/data-table/js/responsive.bootstrap4.min.js"></script>
<script src="{{ asset('public/backend') }}/js/dropify.min.js"></script>
<script src="{{ asset('public/backend') }}/js/bootstrap-datepicker.js"></script>
<script src="{{ asset('public/backend') }}/js/jquery.validate.min.js"></script>
<script src="{{ asset('public/backend') }}/js/summernote.min.js"></script>
<script src="{{ asset('public/backend') }}/js/toastr.js"></script>
<script src="{{ asset('public/backend') }}/js/print.js"></script>
<!-- Main JS-->
<script src="{{ asset('public/backend') }}/js/main.js"></script>
<script src="{{ asset('public/backend') }}/js/script.js"></script>
@yield('js-script')
<script type="text/javascript">
    $(document).ready(function(){  
        @if( ! Request::is('dashboard'))
        $(".form-header h3").html($(".title").html()); 
        $(".form-header h3").html($(".card-title").html());
        @endif
        @if(Session::has('success'))
        Command: toastr["success"]("{{session('success')}}")
        @endif
        @if(Session::has('error'))
        Command: toastr["error"]("{{session('error')}}")
        @endif
    });
</script>
</html>
<!-- end document-->
