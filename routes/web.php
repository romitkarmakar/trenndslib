<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::group(['middleware' => ['install']], function () {

	Route::get('/', function () {
		return redirect('/dashboard');
	});

	Auth::routes();

	Route::group(['middleware' => ['auth']], function () {

		Route::any('/dashboard', 'DashboardController@index');

		Route::get('profile/show', 'ProfileController@profile_show');
		Route::get('profile/edit', 'ProfileController@profile_edit');
		Route::post('profile/update', 'ProfileController@profile_update');
		Route::get('password/change', 'ProfileController@password_change');
		Route::any('password/update', 'ProfileController@password_update');

		//only for Admin
		Route::group(['middleware' => ['permission:Admin,Librarian']], function () {
			Route::resource('users','UserController');

			Route::any('/general_settings', 'SettingsController@general_settings');

			Route::any('/notification_settings', 'SettingsController@notification_settings');

			Route::any('/backup_database', 'BackupController@backup_database');
		});

		//only for Admin,Librarian
		Route::group(['middleware' => ['permission:Admin,Librarian']], function () {
			Route::get('library/{page?}', 'LibraryController@index');

			Route::group(['prefix' => 'library'], function () {
				Route::resource('book_categories','BookCategoryController');

				Route::resource('books','BookController');

				Route::get('members/library_card/{id}', 'MemberController@library_card')->name('members.library_card');
				Route::resource('members','MemberController');

				Route::get('circulations/book_return_info/{id}', 'CirculationController@book_return_info');
				Route::post('circulations/book_return/{id}', 'CirculationController@book_return');
				Route::post('circulations/search', 'CirculationController@search');
				Route::get('circulations/list/{member_id}', 'CirculationController@index');
				Route::resource('circulations','CirculationController');
			});

			Route::get('notifications/create/{id}', 'NotificationController@create');
			Route::resource('notifications','NotificationController');

			Route::get('book_requests', 'BookRequestController@index');
			Route::get('book_requests/{id}', 'BookRequestController@details')->name('book_requests.show');
			Route::get('book_requests/status/{id}/{status?}', 'BookRequestController@status');
		});

		//only for members
		Route::group(['middleware' => ['permission:Member']], function () {
			Route::get('books', 'BookController@index');
			Route::get('books/{id}', 'BookController@details');

			Route::get('my_circulations', 'CirculationController@my_circulations');
			Route::get('my_circulations/{id}', 'CirculationController@details')->name('my_circulations.show');

			Route::resource('my_book_requests','BookRequestController');
			Route::get('my_book_requests', 'BookRequestController@my_book_requests');

			Route::get('my_notifications', 'NotificationController@my_notifications');
			Route::get('my_notifications/{id}', 'NotificationController@details');
		});
	});
});

//cronjon url
Route::any('automatic_notify', 'NotificationController@automatic_notify');

//installation
Route::get('/installation', 'Install\InstallController@index');
Route::get('install/database', 'Install\InstallController@database');
Route::post('install/process_install', 'Install\InstallController@process_install');
Route::get('install/create_user', 'Install\InstallController@create_user');
Route::post('install/store_user', 'Install\InstallController@store_user');
Route::get('install/system_settings', 'Install\InstallController@system_settings');
Route::post('install/finish', 'Install\InstallController@final_touch');
