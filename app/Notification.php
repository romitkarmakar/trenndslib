<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{

	public function circulation() 
	{
		return $this->belongsTo('App\Circulation');
	}

	public function user() 
	{
		return $this->belongsTo('App\User', 'sender_id');
	}
}
