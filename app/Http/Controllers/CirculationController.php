<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Circulation;

class CirculationController extends Controller
{
    /**
     * Instantiate a new PostController instance.
     *
     * @return void
     */
    public function __construct()
    {
        // Timezone
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        return redirect('library/circulations/list/' . $request->member_id);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($member_id)
    {   
        $member = \App\Member::where('member_id', $member_id)->first();
        $circulations = Circulation::where('member_id', $member->id)->orderBy('id', 'DESC')->get();
        return view('backend.library.circulations.index', compact('member_id','member', 'circulations'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function my_circulations()
    {
        $circulations = Circulation::where('member_id', get_member_id())->orderBy('id', 'DESC')->get();
        return view('backend.my_circulations.index', compact('circulations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if( ! $request->ajax()){
           return view('backend.library.circulations.create');
        }else{
           return view('backend.library.circulations.modal.create');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'member_id' => 'required',
            'book_id' => 'required',
            'issue_date' => 'required|date',
            'due_date' => 'required|date',
        ]);
        
        if ($validator->fails()) {
            if($request->ajax()){ 
                return response()->json(['result'=>'error','message'=>$validator->errors()->all()]);
            }else{
                return back()->withErrors($validator)->withInput();
            }           
        }

        $book_issue = new Circulation();
        $book_issue->member_id = $request->member_id;
        $book_issue->book_id = $request->book_id;
        $book_issue->issue_date = $request->issue_date;
        $book_issue->due_date = $request->due_date;
        $book_issue->note = $request->note;
        $book_issue->save();

        if(! $request->ajax()){
           return back()->with('success', 'Information has been added');
        }else{
           return response()->json(['result'=>'success','message'=> 'Information has been added', 'redirect' => url('library/circulations/')]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $circulation = Circulation::find($id);
        if( ! $request->ajax()){
           return view('backend.library.circulations.show', compact('circulation'));
        }else{
           return view('backend.library.circulations.modal.show', compact('circulation'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function details(Request $request, $id)
    {
        $circulation = Circulation::find($id);
        if( ! $request->ajax()){
           return view('backend.my_circulations.show', compact('circulation'));
        }else{
           return view('backend.my_circulations.modal.show', compact('circulation'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $book_issue = Circulation::find($id);
        if( ! $request->ajax()){
           return view('backend.library.circulations.edit', compact('book_issue'));
        }else{
           return view('backend.library.circulations.modal.edit', compact('book_issue'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = \Validator::make($request->all(), [
            'member_id' => 'required',
            'book_id' => 'required',
            'issue_date' => 'required|date',
            'due_date' => 'required|date',
        ]);
        
        if ($validator->fails()) {
            if($request->ajax()){ 
                return response()->json(['result'=>'error','message'=>$validator->errors()->all()]);
            }else{
                return back()->withErrors($validator)->withInput();
            }           
        }

        $book_issue = Circulation::find($id);
        $book_issue->member_id = $request->member_id;
        $book_issue->book_id = $request->book_id;
        $book_issue->issue_date = $request->issue_date;
        $book_issue->due_date = $request->due_date;
        $book_issue->note = $request->note;
        $book_issue->save();

        if(! $request->ajax()){
           return redirect('library/circulations')->with('success', 'Information has been updated');
        }else{
           return response()->json(['result'=>'success','message'=> 'Information has been updated', 'redirect' => url()->previous()]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function book_return_info(Request $request, $id)
    {
        $book_issue = Circulation::find($id);
        if( ! $request->ajax()){
           return view('backend.library.circulations.return', compact('book_issue'));
        }else{
           return view('backend.library.circulations.modal.return', compact('book_issue'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function book_return(Request $request, $id)
    {
        $validator = \Validator::make($request->all(), [
            'return_date' => 'required|date',
        ]);
        
        if ($validator->fails()) {
            if($request->ajax()){ 
                return response()->json(['result'=>'error','message'=>$validator->errors()->all()]);
            }else{
                return back()->withErrors($validator)->withInput();
            }           
        }

        $book_issue = Circulation::find($id);
        $book_issue->return_date = $request->return_date;
        $book_issue->is_return = 1;
        $book_issue->save();

        if(! $request->ajax()){
           return redirect('library/circulations')->with('success', 'Information has been updated');
        }else{
           return response()->json(['result'=>'success','message'=> 'Information has been updated', 'redirect' => url()->previous()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $book_issue = Circulation::find($id);
        $book_issue->delete();

        return back()->with('success', 'Information has been deleted');
    }
}
