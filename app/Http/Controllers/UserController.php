<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\User;
use Auth;

class UserController extends Controller
{
    /**
     * Instantiate a new PostController instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::whereIn('role', ['Admin', 'Librarian'])->orderBy('id', 'DESC')->get();
        return view('backend.users.index',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if( ! $request->ajax()){
           return view('backend.users.create');
        }else{
           return view('backend.users.modal.create');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'name' => 'required|max:191',
            'email' => 'unique:users|required|max:191',
            'role' => 'required',
            'address' => 'required',
            'password' => 'required|min:6|confirmed',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:5120',
        ]);
        
        if ($validator->fails()) {
            if($request->ajax()){ 
                return response()->json(['result'=>'error','message'=>$validator->errors()->all()]);
            }else{
                return redirect('users/create')
                            ->withErrors($validator)
                            ->withInput();
            }           
        }

        $user = new User();
        $user->name = $request->name;
        $user->phone = $request->phone;
        $user->email = $request->email;
        $user->role = $request->role;
        $user->address = $request->address;
        $user->password = \Hash::make($request->password);
        
        if($request->hasFile('image')){
            $file = $request->file('image');
            $file_name = rand().'.'.$file->getClientOriginalExtension();
            \Image::make($file)->resize(160, 160)->save(public_path('uploads/images/users/') . $file_name);
            $user->image = 'users/' . $file_name;
        }
        
        $user->save();

        if(! $request->ajax()){
           return redirect('users/create')->with('success', 'Information has been added');
        }else{
           return response()->json(['result'=>'success','message'=> 'Information has been added']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $user = User::find($id);
        if( ! $request->ajax()){
           return view('backend.users.show', compact('user'));
        }else{
           return view('backend.users.modal.show', compact('user'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $user = User::find($id);
        if( ! $request->ajax()){
           return view('backend.users.edit', compact('user'));
        }else{
           return view('backend.users.modal.edit', compact('user'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = \Validator::make($request->all(), [
            'name' => 'required|max:191',
            'email' => [
                'required',
                Rule::unique('users')->ignore($id),
            ],
            'role' => 'required',
            'address' => 'required',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:5120',
        ]);
        
        if ($validator->fails()) {
            if($request->ajax()){ 
                return response()->json(['result'=>'error','message'=>$validator->errors()->all()]);
            }else{
                return back()->withErrors($validator)->withInput();
            }           
        }

        $user = User::find($id);
        $user->name = $request->name;
        $user->phone = $request->phone;
        $user->email = $request->email;
        $user->role = $request->role;
        $user->address = $request->address;
        if($request->hasFile('image')){
            $file = $request->file('image');
            $file_name = rand().'.'.$file->getClientOriginalExtension();
            \Image::make($file)->resize(160, 160)->save(public_path('uploads/images/users/') . $file_name);
            $user->image = 'users/' . $file_name;
        }
        $user->save();

        if(! $request->ajax()){
           return redirect('users')->with('success', 'Information has been updated');
        }else{
           return response()->json(['result'=>'success','message'=> 'Information has been updated']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();
        return redirect('users')->with('success','Information has been deleted');
    }
}
