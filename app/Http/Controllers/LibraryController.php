<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LibraryController extends Controller
{
    
    /**
     * Instantiate a new PostController instance.
     *
     * @return void
     */
    public function __construct()
    {
        // Timezone
        config(['app.timezone' => app_data('timezone')]);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $book_categories = \App\BookCategory::orderBy('id', 'DESC')->get();
        $books = \App\Book::orderBy('id', 'DESC')->get();
        $members = \App\Member::orderBy('id', 'DESC')->get();
        $circulations = \App\Circulation::orderBy('id', 'DESC')->get();
        
        return view('backend.library.index',compact('book_categories', 'books', 'members', 'circulations'));
    }
}
