<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;
use Image;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $books = Book::orderBy('books.id', 'DESC')->get();
        return view('backend.books.index',compact('books'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if( ! $request->ajax()){
           return view('backend.library.books.create');
        }else{
           return view('backend.library.books.modal.create');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'isbn' => 'nullable|string|max:80',
            'title' => 'required|string|max:191',
            'sub_title' => 'nullable|string|max:191',
            'author' => 'required|string|max:80',
            'edition' => 'required|string|max:80',
            'edition_year' => 'nullable|string|max:80',
            'quantity' => 'required|string|max:11',
            'publisher' => 'nullable|string|max:80',
            'series' => 'nullable|string|max:80',
            'category_id' => 'required|string|max:80',
            'editor' => 'nullable|string|max:80',
            'publication_year' => 'nullable|string|max:80',
            'publication_place' => 'nullable|string|max:80',
            'rack_no' => 'nullable|string|max:80',
            'barcode' => 'nullable|string|max:80',
            'description' => 'nullable|string|max:80',
            'photo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:5120',
        ]);
        
        if ($validator->fails()) {
            if($request->ajax()){ 
                return response()->json(['result'=>'error','message'=>$validator->errors()->all()]);
            }else{
                return back()->withErrors($validator)->withInput();
            }           
        }

        $book = new Book();
        $book->isbn = $request->isbn;
        $book->title = $request->title;
        $book->sub_title = $request->sub_title;
        $book->author = $request->author;
        $book->edition = $request->edition;
        $book->edition_year = $request->edition_year;
        $book->quantity = $request->quantity;
        $book->publisher = $request->publisher;
        $book->series = $request->series;
        $book->category_id = $request->category_id;
        $book->editor = $request->editor;
        $book->publication_year = $request->publication_year;
        $book->publication_place = $request->publication_place;
        $book->rack_no = $request->rack_no;
        $book->barcode = $request->barcode;
        $book->description = $request->description;
        if($request->hasFile('photo')){
            $file = $request->file('photo');
            $file_name = rand().'.'.$file->getClientOriginalExtension();
            \Image::make($file)->resize(160, 160)->save(public_path('uploads/images/books/') . $file_name);
            $book->photo = 'books/' . $file_name;
        }
        $book->save();
        $book->book_id = 'B' . date('Y') . sprintf('%04u', $book->id);
        $book->save();

        if(! $request->ajax()){
           return back()->with('success', 'Information has been added');
        }else{
           return response()->json(['result'=>'success','message'=> 'Information has been added', 'redirect' => url('library/books/')]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $book = Book::find($id);
        if( ! $request->ajax()){
           return view('backend.library.books.show', compact('book'));
        }else{
           return view('backend.library.books.modal.show', compact('book'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function details(Request $request, $id)
    {
        $book = Book::find($id);
        if( ! $request->ajax()){
           return view('backend.books.show', compact('book'));
        }else{
           return view('backend.books.modal.show', compact('book'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $book = Book::find($id);
        if( ! $request->ajax()){
           return view('backend.library.books.edit', compact('book'));
        }else{
           return view('backend.library.books.modal.edit', compact('book'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = \Validator::make($request->all(), [
            'isbn' => 'nullable|string|max:80',
            'title' => 'required|string|max:191',
            'sub_title' => 'nullable|string|max:191',
            'author' => 'required|string|max:80',
            'edition' => 'required|string|max:80',
            'edition_year' => 'nullable|string|max:80',
            'quantity' => 'required|string|max:11',
            'publisher' => 'nullable|string|max:80',
            'series' => 'nullable|string|max:80',
            'category_id' => 'required|string|max:80',
            'editor' => 'nullable|string|max:80',
            'publication_year' => 'nullable|string|max:80',
            'publication_place' => 'nullable|string|max:80',
            'rack_no' => 'nullable|string|max:80',
            'barcode' => 'nullable|string|max:80',
            'description' => 'nullable|string|max:80',
            'photo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:5120',
        ]);
        
        if ($validator->fails()) {
            if($request->ajax()){ 
                return response()->json(['result'=>'error','message'=>$validator->errors()->all()]);
            }else{
                return back()->withErrors($validator)->withInput();
            }           
        }

        $book = Book::find($id);
        $book->isbn = $request->isbn;
        $book->title = $request->title;
        $book->sub_title = $request->sub_title;
        $book->author = $request->author;
        $book->edition = $request->edition;
        $book->edition_year = $request->edition_year;
        $book->quantity = $request->quantity;
        $book->publisher = $request->publisher;
        $book->series = $request->series;
        $book->category_id = $request->category_id;
        $book->editor = $request->editor;
        $book->publication_year = $request->publication_year;
        $book->publication_place = $request->publication_place;
        $book->rack_no = $request->rack_no;
        $book->barcode = $request->barcode;
        $book->description = $request->description;
        if($request->hasFile('photo')){
            $file = $request->file('photo');
            $file_name = rand().'.'.$file->getClientOriginalExtension();
            \Image::make($file)->resize(160, 160)->save(public_path('uploads/images/books/') . $file_name);
            $book->photo = 'books/' . $file_name;
        }
        $book->save();

        if(! $request->ajax()){
           return redirect('library/books')->with('success', 'Information has been updated');
        }else{
           return response()->json(['result'=>'success','message'=> 'Information has been updated', 'redirect' => url('library/books/')]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $book = Book::find($id);
        $book->delete();

        return redirect('library/book_categories')->with('success', 'Information has been deleted');
    }
}
