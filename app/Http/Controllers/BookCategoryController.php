<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BookCategory;

class BookCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect('library/book_categories');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if( ! $request->ajax()){
           return view('backend.library.book_categories.create');
        }else{
           return view('backend.library.book_categories.modal.create');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'category_name' => 'required|string|max:80',
        ]);
        
        if ($validator->fails()) {
            if($request->ajax()){ 
                return response()->json(['result'=>'error','message'=>$validator->errors()->all()]);
            }else{
                return back()->withErrors($validator)->withInput();
            }           
        }

        $category = new BookCategory();
        $category->category_name = $request->category_name;
        $category->save();

        if(! $request->ajax()){
           return back()->with('success', 'Information has been added');
        }else{
           return response()->json(['result'=>'success','message'=> 'Information has been added', 'redirect' => url('library/book_categories/')]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $category = BookCategory::find($id);
        if( ! $request->ajax()){
           return view('backend.library.book_categories.edit', compact('category'));
        }else{
           return view('backend.library.book_categories.modal.edit', compact('category'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = \Validator::make($request->all(), [
            'category_name' => 'required|string|max:80',
        ]);
        
        if ($validator->fails()) {
            if($request->ajax()){ 
                return response()->json(['result'=>'error','message'=>$validator->errors()->all()]);
            }else{
                return back()->withErrors($validator)->withInput();
            }           
        }

        $category = BookCategory::find($id);
        $category->category_name = $request->category_name;
        $category->save();

        if(! $request->ajax()){
           return redirect('library/book_categories')->with('success', 'Information has been updated');
        }else{
           return response()->json(['result'=>'success','message'=> 'Information has been updated', 'redirect' => url('library/book_categories/')]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = BookCategory::find($id);
        $category->delete();

        return redirect('library/book_categories')->with('success', 'Information has been deleted');
    }
}
