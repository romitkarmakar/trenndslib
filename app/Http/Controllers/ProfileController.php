<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\User;
use Auth;

class ProfileController extends Controller
{

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function profile_show(Request $request)
    {
        $user = User::find(Auth::user()->id);

        if( ! $request->ajax()){
            return view('backend.profile.show', compact('user'));
        }else{
            return view('backend.profile.modal.show', compact('user'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function profile_edit(Request $request)
    {
        $user = User::find(Auth::user()->id);

        if( ! $request->ajax()){
            return view('backend.profile.edit', compact('user'));
        }else{
            return view('backend.profile.modal.edit', compact('user'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function profile_update(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'name' => 'required|max:191',
            'email' => [
                'required',
                Rule::unique('users')->ignore(Auth::user()->id),
            ],
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:5120',
        ]);
        
        if ($validator->fails()) {
            if($request->ajax()){ 
                return response()->json(['result'=>'error','message'=>$validator->errors()->all()]);
            }else{
                return back()->withErrors($validator)->withInput();
            }           
        }
        $user = User::find(Auth::user()->id);
        $user->name = $request->name;
        $user->phone = $request->phone;
        $user->email = $request->email;
        $user->address = $request->address;
        if($request->hasFile('image')){
            $file = $request->file('image');
            $file_name = rand().'.'.$file->getClientOriginalExtension();
            \Image::make($file)->resize(160, 160)->save(public_path('uploads/images/users/') . $file_name);
            $user->image = 'users/' . $file_name;
        }
        $user->save();

        if(! $request->ajax()){
            return back()->with('success', 'Information has been updated');
        }else{
            return response()->json(['result'=>'success','message'=> 'Information has been updated']);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function password_change(Request $request)
    {
        if( ! $request->ajax()){
        return view('backend.profile.password_change');
        }else{
            return view('backend.profile.modal.password_change');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function password_update(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'oldpassword' => 'required',
            'password' => 'required|min:6|confirmed',
        ]);
        
        if ($validator->fails()) {
            if($request->ajax()){ 
                return response()->json(['result'=>'error','message'=>$validator->errors()->all()]);
            }else{
                return back()->withErrors($validator)->withInput();
            }           
        }
        
        $user = User::find(Auth::User()->id);
        if(\Hash::check($request->oldpassword, $user->password)){
            $user->password = \Hash::make($request->password);
            $user->save();
        }else{
            if(! $request->ajax()){
                return back()->withInput(\Input::all())->with('error','Old password not match');
            }else{
                return response()->json(['result'=>'error','message'=> 'Old password not match']);
            }
        }
        if(! $request->ajax()){
            return redirect('dashboard')->with('success', 'Password has been changed');
        }else{
            return response()->json(['result'=>'success','message'=> 'Password has been changed']);
        }
    }
}
