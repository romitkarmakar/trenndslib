<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\BookRequest;
use App\Member;
use App\Notification;
use App\User;

class MemberController extends Controller
{
    /**
     * Instantiate a new PostController instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if( ! $request->ajax()){
           return view('backend.library.members.create');
        }else{
           return view('backend.library.members.modal.create');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'name' => 'required|max:191',
            'email' => 'unique:users|required|max:191',
            'member_type' => 'required|max:191',
            'address' => 'required',
            'password' => 'required|min:6|confirmed',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:5120',
        ]);
        
        if ($validator->fails()) {
            if($request->ajax()){ 
                return response()->json(['result'=>'error','message'=>$validator->errors()->all()]);
            }else{
                return back()->withErrors($validator)->withInput();
            }           
        }

        $user = new User();
        $user->name = $request->name;
        $user->phone = $request->phone;
        $user->email = $request->email;
        $user->role = 'Member';
        $user->address = $request->address;
        $user->password = \Hash::make($request->password);
        
        if($request->hasFile('image')){
            $file = $request->file('image');
            $file_name = rand().'.'.$file->getClientOriginalExtension();
            \Image::make($file)->resize(160, 160)->save(public_path('uploads/images/members/') . $file_name);
            $user->image = 'members/' . $file_name;
        }
        $user->save();

        $member = new Member();
        $member->user_id = $user->id;
        $member->member_type = $request->member_type;
        $member->save();

        $member->member_id = 'M' . $request->member_type[0] . date('Y') . sprintf('%04u', $member->id);
        $member->save();

        if(! $request->ajax()){
           return back()->with('success', 'Information has been added');
        }else{
           return response()->json(['result'=>'success','message'=> 'Information has been added', 'redirect' => url('library/members/')]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $member = Member::where('members.id',$id)->first();
        if( ! $request->ajax()){
           return view('backend.library.members.show', compact('member'));
        }else{
           return view('backend.library.members.modal.show', compact('member'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function library_card(Request $request, $id)
    {
        $member = Member::where('members.id',$id)->first();
        if( ! $request->ajax()){
           return view('backend.library.members.library_card', compact('member'));
        }else{
           return view('backend.library.members.modal.library_card', compact('member'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $member = Member::join('users', 'users.id', '=', 'user_id')
                            ->select('*', 'members.id AS id')
                            ->where('members.id',$id)
                            ->first();
        if( ! $request->ajax()){
           return view('backend.library.members.edit', compact('member'));
        }else{
           return view('backend.library.members.modal.edit', compact('member'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $member = Member::find($id);
        $validator = \Validator::make($request->all(), [
            'name' => 'required|max:191',
            'email' => [
                'required',
                Rule::unique('users')->ignore($member->user_id),
            ],
            'member_type' => 'required|max:191',
            'address' => 'required',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:5120',
        ]);
        
        if ($validator->fails()) {
            if($request->ajax()){ 
                return response()->json(['result'=>'error','message'=>$validator->errors()->all()]);
            }else{
                return back()->withErrors($validator)->withInput();
            }           
        }

        $member->member_type = $request->member_type;
        $member->save();

        $user = User::find($member->user_id);
        $user->name = $request->name;
        $user->phone = $request->phone;
        $user->email = $request->email;
        $user->address = $request->address;
        if($request->hasFile('image')){
            $file = $request->file('image');
            $file_name = rand().'.'.$file->getClientOriginalExtension();
            \Image::make($file)->resize(160, 160)->save(public_path('uploads/images/members/') . $file_name);
            $user->image = 'members/' . $file_name;
        }
        $user->save();

        if(! $request->ajax()){
           return redirect('library/members')->with('success', 'Information has been updated');
        }else{
           return response()->json(['result'=>'success','message'=> 'Information has been updated', 'redirect' => url('library/members/')]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //destroy member's all data
        
        $circulations = \App\Circulation::where('member_id', $id);

        $circulation_ids = $circulations->pluck('id')->toArray();
        $notifications = Notification::whereIn('circulation_id', $circulation_ids)->get();
        foreach ($notifications as $data) {
            $data->delete();
        }

        foreach ($circulations->get() as $data) {
            $data->delete();
        }

        foreach (BookRequest::where('member_id', $id)->get() as $data) {
            $data->delete();
        }

        $member = Member::find($id);
        $member->delete();

        $user = User::find($member->user_id);
        $user->delete();
        
        return redirect('library/members')->with('success','Information has been deleted');
    }
}
