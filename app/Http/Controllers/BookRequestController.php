<?php

namespace App\Http\Controllers;

use App\BookRequest;
use Illuminate\Http\Request;

class BookRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $book_requests = \App\BookRequest::orderBy('id', 'DESC')->get();
        return view('backend.book_requests.index',compact('book_requests'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function my_book_requests()
    {
        $book_requests = BookRequest::where('member_id', get_member_id())->orderBy('id', 'DESC')->get();
        return view('backend.my_book_requests.index',compact('book_requests'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if( ! $request->ajax()){
           return view('backend.my_book_requests.create');
        }else{
           return view('backend.my_book_requests.modal.create');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'book_title' => 'required|string|max:191',
            'author_name' => 'required|string|max:191',
            'edition' => 'required|string|max:80',
            'year' => 'required|string|max:10',
        ]);
        
        if ($validator->fails()) {
            if($request->ajax()){ 
                return response()->json(['result'=>'error','message'=>$validator->errors()->all()]);
            }else{
                return back()->withErrors($validator)->withInput();
            }           
        }

        $book_request = new BookRequest();
        $book_request->member_id = get_member_id();
        $book_request->book_title = $request->book_title;
        $book_request->author_name = $request->author_name;
        $book_request->edition = $request->edition;
        $book_request->year = $request->year;
        $book_request->note = $request->note;
        $book_request->save();

        if(! $request->ajax()){
           return back()->with('success', 'Information has been added');
        }else{
           return response()->json(['result'=>'success','message'=> 'Information has been added', 'redirect' => url('my_book_requests')]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function details(Request $request, $id)
    {
        $book_request = BookRequest::find($id);
        if( ! $request->ajax()){
           return view('backend.book_requests.show', compact('book_request'));
        }else{
           return view('backend.book_requests.modal.show', compact('book_request'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $book_request = BookRequest::find($id);
        if( ! $request->ajax()){
           return view('backend.my_book_requests.show', compact('book_request'));
        }else{
           return view('backend.my_book_requests.modal.show', compact('book_request'));
        }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $book_request = BookRequest::find($id);
        if( ! $request->ajax()){
           return view('backend.my_book_requests.edit', compact('book_request'));
        }else{
           return view('backend.my_book_requests.modal.edit', compact('book_request'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = \Validator::make($request->all(), [
            'book_title' => 'required|string|max:191',
            'author_name' => 'required|string|max:191',
            'edition' => 'required|string|max:80',
            'year' => 'required|string|max:10',
        ]);
        
        if ($validator->fails()) {
            if($request->ajax()){ 
                return response()->json(['result'=>'error','message'=>$validator->errors()->all()]);
            }else{
                return back()->withErrors($validator)->withInput();
            }           
        }

        $book_request = BookRequest::find($id);
        $book_request->book_title = $request->book_title;
        $book_request->author_name = $request->author_name;
        $book_request->edition = $request->edition;
        $book_request->year = $request->year;
        $book_request->note = $request->note;
        $book_request->save();

        if(! $request->ajax()){
           return redirect('my_book_requests')->with('success', 'Information has been updated');
        }else{
           return response()->json(['result'=>'success','message'=> 'Information has been updated', 'redirect' => url('my_book_requests')]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param  int  $status
     * @return \Illuminate\Http\Response
     */

    public function status($id, $status)
    {
        if($status == 'Accept'){
            $status = 1;
        }elseif($status == 'Reject'){
            $status = 2;
        }
        $book_request = BookRequest::find($id);
        $book_request->status = $status;
        $book_request->save();
        return redirect('book_requests')->with('success', 'Information has been updated');
    }
}
