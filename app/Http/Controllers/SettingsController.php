<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Setting;
use Carbon\Carbon;

class SettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function general_settings(Request $request)
    {
        
        if($request->all() == null){
           return view('backend.settings.general');
        }else{     
            foreach($request->all() as $key => $value){
               if($key == "_token"){
                   continue;
               }
               if($value != ''){
                    $data = array();
                    $data['value'] = $value; 
                    $data['updated_at'] = Carbon::now();
                    if ($request->hasFile($key)) {
                        $image = $request->file($key);
                        $name = $key . '.' .$image->getClientOriginalExtension();
                        $path = public_path('/uploads/images/');
                        if($key == 'logo'){
                            $name = 'logo.png';
                            \Image::make($image)->resize(175, 50)->save($path . $name);
                        }else{
                            $image->move($path, $name);
                        }
                        $data['value'] = $name; 
                        $data['updated_at'] = Carbon::now();
                    }
                    if(Setting::where('name', $key)->exists()){                
                        Setting::where('name','=',$key)->update($data);         
                    }else{
                        $data['name'] = $key; 
                        $data['created_at'] = Carbon::now();
                        Setting::insert($data); 
                    }
                }
            
            } //End Loop
            return redirect('general_settings')->with('success', 'Saved sucessfully');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function notification_settings(Request $request)
    {
        if($request->all() == array()){
            return view('backend.settings.notification');
        }else{
            foreach($request->all() as $key => $value){
               if($key == "_token"){
                   continue;
               }
               if($value != ''){
                    $data = array();
                    $data['value'] = $value; 
                    $data['updated_at'] = Carbon::now();
                    if(Setting::where('name', $key)->exists()){                
                        Setting::where('name','=',$key)->update($data);         
                    }else{
                        $data['name'] = $key; 
                        $data['created_at'] = Carbon::now();
                        Setting::insert($data); 
                    }
                }
            } //End Loop
            return redirect('notification_settings')->with('success', 'Saved sucessfully');
        }
    }
}
