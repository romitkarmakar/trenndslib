<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class BackUpController extends Controller
{
	public function __construct(){
		header('Cache-Control: no-cache');
		header('Pragma: no-cache');
	}
	
    public function backup_database(){
		@ini_set('max_execution_time', 0);
		@set_time_limit(0);
			
		$return = "";
		$database = 'Tables_in_'.DB::getDatabaseName();
		$tables = array();
		$result = DB::select("SHOW TABLES");

		foreach($result as $table){
			$tables[] = $table->$database;
		}


		//loop through the tables
		foreach($tables as $table){			
			$return .= "DROP TABLE IF EXISTS $table;";

			$result2 = DB::select("SHOW CREATE TABLE $table");
			$row2 = $result2[0]->{'Create Table'};

			$return .= "\n\n".$row2.";\n\n";
			
			$result = DB::select("SELECT * FROM $table");

			foreach($result as $row){	
				$return .= "INSERT INTO $table VALUES(";
				foreach($row as $key=>$val){	
					$return .= "'".addslashes($val)."'," ;	
				}
				$return = substr_replace($return, "", -1);
				$return .= ");\n";
			}
   
			$return .= "\n\n\n";
		}

		$file = public_path('backup/database-'.time().'.sql');
		\File::put($file, $return);
		
		return response()->download($file);
		return redirect()->back()->with('success', 'Backup Created Sucessfully');		
	}
}
