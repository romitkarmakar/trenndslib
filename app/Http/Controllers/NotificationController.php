<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Notification;
use App\Setting;
use Carbon\Carbon;

class NotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $notifications = Notification::WhereIn('type',[1,3])->orderBy('id', 'DESC')->get();
        $emails = Notification::WhereIn('type',[2,3])->orderBy('id', 'DESC')->get();
        return view('backend.notifications.index', compact('notifications', 'emails'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function my_notifications()
    {
        // show only own notifications get circulation_ids by get_member_id()
        $circulation_ids = \App\Circulation::where('member_id', get_member_id())
                                            ->pluck('id')
                                            ->toArray();
        $notifications = Notification::whereIn('circulation_id', $circulation_ids)
                                        ->orderBy('id', 'DESC')
                                        ->get();
        return view('backend.my_notifications.index', compact('notifications'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, $circulation_id)
    {
        if( ! $request->ajax()){
           return view('backend.notifications.create', compact('circulation_id'));
        }else{
           return view('backend.notifications.modal.create', compact('circulation_id'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $notification = new Notification();
        $notification->circulation_id = $request->circulation_id;
        $notification->subject = $request->subject;
        $notification->message = $request->message;
        $notification->type = $request->type;
        $notification->sender_id = \Auth::user()->id;
        $notification->save();

        if(($request->type == 2) || ($request->type == 3)){
            \App\Helpers\ConfigEmail::load('Settings');
            $mail = new \stdClass();
            $mail->subject = $request->subject;
            $mail->message = get_message($notification->id);
            \Mail::to($notification->circulation->member->user->email)->send(new \App\Mail\PrimaryMail($mail));
        }

        if(! $request->ajax()){
           return back()->with('success', 'Message has been sended');
        }else{
           return response()->json(['result'=>'success','message'=> 'Message has been sended', 'redirect' => url()->previous()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $notification = Notification::find($id);
        if( ! $request->ajax()){
           return view('backend.notifications.show', compact('notification'));
        }else{
           return view('backend.notifications.modal.show', compact('notification'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function details(Request $request, $id)
    {
        // show only own notifications get circulation_ids by get_member_id()
        // this query only for best security
        $circulation_ids = \App\Circulation::where('member_id', get_member_id())
                                            ->pluck('id')
                                            ->toArray();
        $notification = Notification::whereIn('circulation_id', $circulation_ids)
                                        ->where('id', $id)
                                        ->first();
        if(is_null($circulation_ids) || is_null($notification)){
            return redirect('my_notifications')->with('error', 'You cant see other notification');
        }
        $notification->status = 1;
        $notification->save();

        return view('backend.my_notifications.show', compact('notification'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function automatic_notify()
    {
        if(app_data('auto_notify_delayed_members') == 1){
            @ini_set('max_execution_time', 0);
            @set_time_limit(0);

            $toDay = Carbon::now()->toDateString();
            $circulations = \App\Circulation::whereDate('due_date', '<', $toDay)->where(['is_return' => 0])->get();
            foreach($circulations AS $circulation){
                $notification = new Notification();
                $notification->circulation_id = $circulation->id;
                $notification->subject = app_data('notification_subject');
                $notification->message = app_data('notification_message');
                $notification->type = app_data('notification_type');
                $notification->sender_id = \App\User::where('role', 'Admin')->first()->id;
                $notification->save();

                if((app_data('notification_type') == 2) || (app_data('notification_type') == 3)){
                    \App\Helpers\ConfigEmail::load('Settings');
                    $mail = new \stdClass();
                    $mail->subject = app_data('notification_subject');
                    $mail->message = get_message($notification->id);
                    \Mail::to($circulation->member->user->email)->send(new \App\Mail\PrimaryMail($mail));
                }
            }
        }
    }
}
