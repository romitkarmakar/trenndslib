<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->role == 'Admin'){
            return $this->admin();
        }elseif(Auth::user()->role == 'Librarian'){
            return $this->librarian();
        }elseif(Auth::user()->role == 'Member'){
            return $this->member();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function admin()
    {
        $circulations = \App\Circulation::orderBy('id', 'DESC')->get();
        return view('backend.dashboard.admin', compact('circulations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function librarian()
    {
        $circulations = \App\Circulation::orderBy('id', 'DESC')->get();
        return view('backend.dashboard.librarian', compact('circulations'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function member()
    {
        $circulations = \App\Circulation::where('member_id', get_member_id())->orderBy('id', 'DESC')->get();
        return view('backend.dashboard.member', compact('circulations'));
    }

}
