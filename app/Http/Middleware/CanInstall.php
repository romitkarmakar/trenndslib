<?php

namespace App\Http\Middleware;

use Closure;

class CanInstall
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
		if(env('APP_INSTALLED',false) == true){
		    // Timezone
            date_default_timezone_set(app_data('timezone'));
			return $next($request);
		}
		return redirect('/installation');
    }
}
