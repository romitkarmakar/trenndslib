<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;

class Permission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, ... $roles)
    {
        if ( ! in_array(\Auth::user()->role, $roles)) {
            if( ! $request->ajax()){
               return back()->with('error', 'Sorry, You dont have permission to perform this action !');
            }else{
                return new Response('<h5 class="text-center red">Sorry, You dont have permission to perform this action !</h5>');
            }
        }

        return $next($request);
    }
}
