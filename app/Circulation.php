<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Circulation extends Model
{
    public function member() 
	{
		return $this->belongsTo('App\Member');
	}

	public function book() 
	{
		return $this->belongsTo('App\Book');
	}
}
