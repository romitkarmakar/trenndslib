<?php


if ( ! function_exists('app_data')){
	function app_data($name, $default = '') 
	{
		$setting = DB::table('settings')->where('name', $name)->get();
		if ( ! $setting->isEmpty() ) {
			return $setting[0]->value;
		}
		return $default;

	}
}

if ( ! function_exists('counter')){
	function counter($table, $conditions = null) 
	{
		if (is_null($conditions)) {
			$result = DB::table($table)->count();
		}else{
			$result = DB::table($table)->where($conditions)->count();
		}
		return $result;
	}
}

if ( ! function_exists('get_message')){
	function get_message($id) 
	{
		$notification = App\Notification::find($id);
		$member_name = $notification->circulation->member->user->name;
		$member_id = $notification->circulation->member->member_id;
		$book_title = $notification->circulation->book->title;
		$book_id = $notification->circulation->book->book_id;
		$issue_date = $notification->circulation->issue_date;
		$due_date = $notification->circulation->due_date;

		$message = $notification->message;
		eval("\$message = \"$message\";");
		return $message. "\n";
	}
}

if (! function_exists('create_option')) {
    function create_option($table = '',$value = '',$show = '',$selected = '',$placeholder = '',$orderby = '',$where = null) {
    	if($where !=null){
    		$results = DB::table($table)->where($where)->orderBy('id',($orderby != 'ASC') ? 'DESC' : 'ASC')->get();
    	}else{
    		$results = DB::table($table)->orderBy('id',($orderby != 'ASC') ? 'DESC' : 'ASC')->get();
    	}
        $option = '';
        if($placeholder !='hide_placeholder'){
            if($placeholder != ''){
                $placeholder = $placeholder;
            }else{
                $placeholder = 'Select One';
            }
            $option = '<option value="">'.$placeholder.'</option>';
        }
        foreach ($results as $data) {
        	if($data->$value == $selected){
        		$option.="<option value='".$data->$value."' selected='true'>".ucwords($data->$show)."</option>";
        	}else{
        		$option.= "<option value='".$data->$value."'>".ucwords($data->$show)."</option>";
        	}
        }
        echo $option;
    }
}

if (! function_exists('create_member_option')) {
    function create_member_option($value = 'id') {
    	$results = \App\Member::orderBy('id','DESC')->get();
        $option = '<option value="">Select One</option>';
        foreach ($results as $data) {
        	$option.='<option value="' . $data->$value . '">' . $data->member_id . ' - ' .$data->user->name . "</option>";
        }
        echo $option;
    }
}

if ( ! function_exists('get_member_id')){
	function get_member_id() 
	{
		$member_id = \App\Member::where(['user_id' => \Auth::user()->id])->first()->id;
		return isset($member_id) ? $member_id : '';
	}
}


if ( ! function_exists('new_notifications')){
	function new_notifications() 
	{
		// show only own notifications get circulation_ids by get_member_id()
        $circulation_ids = \App\Circulation::where('member_id', get_member_id())
                                            ->pluck('id')
                                            ->toArray();
        $notifications = \App\Notification::whereIn('circulation_id', $circulation_ids)
        									->where('status', 0)
        									->orderBy('id', 'DESC')
        									->get();
		return $notifications;
	}
}

if ( ! function_exists('time_elapsed_string')){
	function time_elapsed_string($datetime, $full = false) 
	{
		$now = new \DateTime;
		$ago = new \DateTime($datetime);
		$diff = $now->diff($ago);

		$diff->w = floor($diff->d / 7);
		$diff->d -= $diff->w * 7;

		$string = array(
			'y' => 'year',
			'm' => 'month',
			'w' => 'week',
			'd' => 'day',
			'h' => 'hour',
			'i' => 'minute',
			's' => 'second',
		);
		foreach ($string as $k => &$v) {
			if ($diff->$k) {
				$v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
			} else {
				unset($string[$k]);
			}
		}

		if (!$full) $string = array_slice($string, 0, 1);
		return $string ? implode(', ', $string) . ' ago' : 'just now';
	}
}



if ( ! function_exists('timezone_list'))
{

	function timezone_list() {
		$zones_array = array();
		$timestamp = time();
		foreach(timezone_identifiers_list() as $key => $zone) {
			date_default_timezone_set($zone);
			$zones_array[$key]['ZONE'] = $zone;
			$zones_array[$key]['GMT'] = 'UTC/GMT ' . date('P', $timestamp);
		}
		return $zones_array;
	}

}

if ( ! function_exists('create_timezone_option'))
{

	function create_timezone_option($old="") {
		$option = "";
		$timestamp = time();
		foreach(timezone_identifiers_list() as $key => $zone) {
			date_default_timezone_set($zone);
			$selected = $old == $zone ? "selected" : "";
			$option .= '<option value="'. $zone .'"'.$selected.'>'. 'GMT ' . date('P', $timestamp) .' '.$zone.'</option>';
		}
		echo $option;
	}

}


if ( ! function_exists( 'get_country_list' ))
{
	function get_country_list( $old_data='' ) {
		if( $old_data == "" ){
			echo file_get_contents( app_path().'/Helpers/country.txt' );
		}else{
			$pattern='<option value="'.$old_data.'">';
			$replace='<option value="'.$old_data.'" selected="selected">';
			$country_list=file_get_contents( app_path().'/Helpers/country.txt' );
			$country_list=str_replace($pattern,$replace,$country_list);
			echo $country_list;
		}
	}	
}
