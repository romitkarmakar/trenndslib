<?php

namespace App\Helpers;

class ConfigEmail
{

    public static function load($type)
    {
        $method = 'load' . ucfirst($type);

        static::$method();
    }

    protected static function loadSettings()
    {
        // Timezone
        config(['app.timezone' => app_data('timezone')]);

        // Email
        config(['mail.driver' => 'smtp']);
        config(['mail.from.name' => app_data('from_name')]);
        config(['mail.from.address' => app_data('from_email')]);
        config(['mail.host' => app_data('smtp_host')]);
        config(['mail.port' => app_data('smtp_port')]);
        config(['mail.username' => app_data('smtp_username')]);
        config(['mail.password' => app_data('smtp_password')]);
        config(['mail.encryption' => app_data('smtp_encryption')]);

    }

}